#!/bin/sh

#build 
cd $(dirname "$0")
mkdir build
cd build
cmake ..
make
cd ..

# generate blob
cd build
./blobsref
cd ..

#install
dir=$(pwd)
mv $dir/build/atlas /usr/local/bin/atlas
mv $dir/build/blobs.txt /usr/local/bin/blobs.txt


