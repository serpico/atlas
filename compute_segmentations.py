import os
import argparse
from subprocess import call

parser = argparse.ArgumentParser('Compute atlas segmentations', description='''ATLAS is a spot detector described in the following article:
Basset, Boulanger, Salamero, Bouthemy, and Kervrann. Adaptive spot detection with optimal scale selection in fluorescence microscopy images. IEEE Trans. Image Processing, 2015.''', formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument('-a', '--atlas', help='Path to the atlas binaries folder', default='build/')
parser.add_argument('-d', '--dataset', help='Path to the folder containing images to segment', required=True)
parser.add_argument('-b', '--blobs', help='Path to the blobs.txt file. The file will be generated automatically if it does not exist already.', default='blobs.txt')
parser.add_argument('-aa', '--atlas_arguments', help='Supplementary arguments. See ATLAS help and paper for more details.', default='-rad 21 -pval 0.001 -arealim 3')
parser.add_argument('-o', '--output', help='Output folder path', default='segmentations/')

args = parser.parse_args()

if args.blobs == 'blobs.txt' and not os.path.exists('blobs.txt'):
    print('Generating blobs.txt...')
    call([os.path.join(args.atlas, 'blobsref')])

path_dataset = args.dataset
path_output = args.output

if not os.path.isdir(path_output):
    os.mkdir(path_output)

for fname in os.listdir(path_dataset):
    if fname.lower().endswith('.tif') or fname.lower().endswith('.tiff'):
        print('Processing ' + fname + ' ...')
        path_img = os.path.join(path_dataset, fname)
        path_seg = os.path.join(path_output, fname)
        
        cmd = [ os.path.join(args.atlas, 'atlas'), '-i', path_img, '-o', path_seg ] + args.atlas_arguments.split(' ')
        call(cmd)