FROM ubuntu:20.04

WORKDIR /app

COPY . /app

RUN apt-get -y update && \
    apt-get -y upgrade && \
    apt-get -y install cmake && \
    apt-get -y install g++ && \
    apt-get -y install libtiff-dev && \
    apt-get -y install libgsl-dev && \ 
    mkdir build && \
    cd build && \
    cmake .. && \
    make && \
    ./blobsref

ENV PATH="/app/build:$PATH"

CMD ["bash"]
