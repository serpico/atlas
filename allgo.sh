#!/bin/sh

# Script that install atlas on Allgo platform:
# 1- create the Allgo app and log to the sand box as described in the allgo documentation https://allgo.gitlabpages.inria.fr/doc/deploy.html
# 2- install git (apt-get update & apt-get install git)
# 3- clone this repository (git clone https://gitlab.inria.fr/serpico/atlas.git) in /home/allgo
# 4- run this script

# dependancies
apt-get install g++
apt-get install cmake
apt-get install libtiff-dev
apt-get install libgsl-dev

# build
cd $(dirname "$0")
mkdir build
cd build
cmake ..
make
cd ..

# generate blob file
echo "generate blob file. May take some time"
build/blobsref -j 4 -o blobs.txt

# entry point
cp allgo_entrypoint /home/allgo/entrypoint
