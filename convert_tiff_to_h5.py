import os
import re
import skimage
import numpy as np
import argparse
from glob import glob1
import h5py

parser = argparse.ArgumentParser('Convert tiff to h5', description='''Convert tiff files generated by atlas to h5 format''', formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument('-s', '--segmentations', help='Path to the folder containing the segmentations generated by atlas', required=True)
parser.add_argument('-o', '--output', help='Output folder path', default='segmentations_h5/')

args = parser.parse_args()

# Writes data in h5 file, to specified h5 dataset. Is also adapted for labelmaps: saved as int8 to gain disk space.
# INPUTS:
#   array    : numpy array
#   filename : string '/path/to/file.h5'
#   dset_name: string dataset name
def write_h5array(array, filename, dset_name='dataset'):
    h5file = h5py.File(filename, 'w')
    if array.dtype == np.int8:
        dset = h5file.create_dataset(dset_name, array.shape, dtype='int8')
        dset[:] = np.int8(array)
    else:
        dset = h5file.create_dataset(dset_name, array.shape, dtype='float16')
        dset[:] = np.float16(array)
    h5file.close()

dset_path = args.segmentations
path_out = args.output

if not os.path.isdir(path_out):
    os.mkdir(path_out)

max_list = []
for root, dirs, files in os.walk(dset_path, topdown=False):
    if root == dset_path: continue
    print(os.path.basename(root))
    # load 1st frame to get image dimensions
    fname_img_test = glob1(root, '*_t1.TIF')
    img_test = skimage.io.imread(os.path.join(root, fname_img_test[0]))
    # Instanciate volume
    nframes = 1001
    vol = np.zeros((nframes, img_test.shape[0], img_test.shape[1]), dtype='uint8')
    
    for name in files:
        if name.startswith('Cell') and name.endswith('.TIF'):
            img = skimage.io.imread(os.path.join(root, name))
            
            slice_idx = re.findall('[0-9]+', name)  # get numbers from fname
            slice_idx = int(slice_idx[-1])  # last number in fname is slice idx
            
            vol[slice_idx-1,:,:] = img
    
    folder_name = os.path.basename(root)
    if not os.path.isdir(folder_name): os.mkdir(folder_name)
    
    folder_path = os.path.join(path_out, folder_name)   
    if not os.path.isdir(folder_path): os.mkdir(folder_path)

    write_h5array(vol, os.path.join(folder_path, 'segmap_atlas.h5'))
            
            
