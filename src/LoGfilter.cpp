/*
 * main.cpp
 *
 *  Created on: Apr 11, 2014
 *      Author: Antoine Basset
 * 
 * Inria Rennes - Bretagne Atlantique
 * Serpico
 * http://serpico.rennes.inria.fr
 */


#include "core.hpp"
#include "optionChecker.hpp"


bool VERBOSE;

int main(int argc, char **argv) {
	using namespace cimg_library;
	using namespace scalespace;
	using namespace atlas;
	TIFFSetWarningHandler(0);


	/* computation options */
#ifdef cimg_use_openmp
	omp_set_nested(0);
	opt_chk::read_thread_nb(argc, argv, false);
#endif

#ifdef DEBUG_MODE
	VERBOSE = true;
#else
	VERBOSE = cimg_option("-v", false, "Verbose mode");
#endif


	/* sequence options */
	const char *in_url   = cimg_option("-i", (char *)0, "Input TIFF file");
	const int first      = cimg_option("-f", 0, "Index of the first frame to compute");
	const int last       = cimg_option("-l", -1, "Index of the first frame to compute (-1 for last frame of the input file)");
	const int step       = cimg_option("-s", 1, "Frame step  (e.g. -s 1 uses all frames, while -s 2 uses one frame over two)");
	const float x0       = cimg_option("-x0", 0.F, "Left margin (<0 for proportion of width)");
	const float y0       = cimg_option("-y0", 0.F, "Top margin (<0 for proportion of height)");
	const float x1       = cimg_option("-x1", -1.F, "Right margin (<0 for proportion of width)");
	const float y1       = cimg_option("-y1", -1.F, "Bottom margin (<0 for proportion of height)");
    const float scale    = cimg_option("-scale", 1.0, "Manual setting of the scale");
	const char *log_url  = cimg_option("-log", "LoG.tif", "Log Image file");

	
	/* help mode */
	if(opt_chk::check_help(argc, argv))
		return EXIT_SUCCESS;

	if(VERBOSE)
		printf("\n");
	/* read sequence */
	if(VERBOSE)
		printf("Reading sequence:\n");
	
	//CImg<float> sequence;
	CImg<float> sequence(in_url);
	
	const int width = sequence.width();
	const int height = sequence.height();
	const int length = sequence.depth();
	
	sequence.crop(
			x0>=0?x0:-x0*width, y0>=0?y0:-y0*height,
			x1>=0?x1:-x1*width-1, y1>=0?y1:-y1*height-1);
	if(VERBOSE) {
		printf("\t%d frame", length);
		if(length>1)
			printf("s");
		printf("\n\t%d x %d px\n\n", sequence.width(), sequence.height());
	}

 
	CImg<float> LoG = scale>0?enhance_objects(sequence, scale):sequence;
	LoG.save(log_url);

	return EXIT_SUCCESS;
}
