/*
 * scalespace.cpp
 *
 *  Created on: Apr 10, 2014
 *      Author: Antoine Basset
 * 
 * Inria Rennes - Bretagne Atlantique
 * Serpico
 * http://serpico.rennes.inria.fr
 */


#include "scalespace.hpp"

cimg_library::CImgList<unsigned char> scalespace::draw_blobs(
		const cimg_library::CImg<float>& img,
		const cimg_library::CImgList<int>& blobs){

	using namespace cimg_library;

	CImgList<unsigned char> visu(img.width(), img.height(), img.depth(), 3, 0);
	const unsigned char color[] = {255,255,255};

	cimglist_for(blobs, l)
		cimg_forX(blobs[l], b)
			visu[blobs[l](b, 2)].draw_circle(blobs[l](b, 0), blobs[l](b, 1), l-1, color);

	return visu;
}
