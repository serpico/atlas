/*
 * evaluation.hpp
 *
 *  Created on: Aug 21, 2014
 *      Author: Antoine Basset
 * 
 * Inria Rennes - Bretagne Atlantique
 * Serpico
 * http://serpico.rennes.inria.fr
 */

#ifndef EVALUATION_HPP_
#define EVALUATION_HPP_

#include <iostream>
#include <fstream>
#include <sstream>
#include "CImg.h"


/**
 * Performance measures indices.
 */
enum measure_idx {
	N_OBJECTS = 0,	//< number of objects in ground-truth
	N_DETECTIONS,	//< number of detections
	N_TP,			//< number of true positives
	N_FP,			//< number of false positives
	N_FN,			//< number of false negatives
	TPR,			//< true positive rate
	FPR,			//< modified false positive rate
	PRECISION,		//< precision
	RECALL,			//< recall
	F_SCORE,		//< F-score
	MEASURES_COUNT	//< number of performance measures
};


cimg_library::CImg<float> mask2centroids(const cimg_library::CImg<bool>& mask);

cimg_library::CImgList<float> read_centroids(const char *url);

cimg_library::CImgList<float> read_txy_table(const char *url);

cimg_library::CImgList<float> read_inr(const char *url);

void discard_centroids(
		cimg_library::CImgList<float>& centroids,
		const cimg_library::CImg<bool>& mask);


cimg_library::CImg<float> eval_2Dt(
		const cimg_library::CImgList<float>& detections,
		const cimg_library::CImgList<float>& gt,
		float tolerance=1);

#endif /* EVALUATION_HPP_ */
