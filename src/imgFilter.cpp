/*
 * imgFilter.cpp
 *
 *  Created on: 15 oct. 2013
 *      Author: abasset
 */


#include "imgFilter.hpp"

using namespace std;
using namespace cimg_library;


namespace sampled_filter {

template<>
CImg<float> gaussian_kernel<0>(const float sigma) {

	/* initialize mask */
	const int M = 4*max(sigma, 1.F)+1;
	const int size = 2*M+1;
	CImg<float> kernel(size);

	/* constants */
	const float sqrt2pi = sqrt(M_PI*2);
	const float s2 = sigma*sigma;
	const float inv_ds2 = .5/s2;
	const float norm = 1.F/(sqrt2pi*sigma);

	/* fill mask */
#pragma omp parallel for
	for(int i=0; i<=M; ++i) {
		const int i2 = i*i;
		const float value = norm*exp(-inv_ds2*i2);
		kernel[M-i] = value;
		kernel[M+i] = value;
	}

	return kernel;
}

template<>
CImg<float> gaussian_kernel<1>(const float sigma, const CImg<float>& Gkernel) {

	/* initialize kernel */
	const int M = Gkernel.size()/2;
	CImg<float> kernel(2*M+1);

	/* constant */
	const float inv_s2 = 1.F/(sigma*sigma);

	/* fill mask */
#pragma omp parallel for
	for(int i=0; i<=M; ++i) {
		const float value = -inv_s2*i*Gkernel[M-i];
		kernel[M-i] = value;
		kernel[M+i] = value;
	}

	return kernel;
}

template<>
CImg<float> gaussian_kernel<1>(const float sigma) {
	return gaussian_kernel<1>(sigma, gaussian_kernel<0>(sigma));
}

template<>
CImg<float> gaussian_kernel<2>(const float sigma, const CImg<float>& Gkernel) {

	/* initialize kernel */
	const int M = Gkernel.size()/2;
	CImg<float> kernel(2*M+1);

	/* constant */
	const float inv_s2 = 1.F/(sigma*sigma);

	/* fill mask */
#pragma omp parallel for
	for(int i=0; i<=M; ++i) {
		const int i2 = i*i;
		const float value = inv_s2*(inv_s2*i2-1)*Gkernel[M-i];
		kernel[M-i] = value;
		kernel[M+i] = value;
	}

	return kernel;
}

template<>
CImg<float> gaussian_kernel<2>(const float sigma) {
	return gaussian_kernel<2>(sigma, gaussian_kernel<0>(sigma));
}

template<>
CImg<float> gaussian_kernel<3>(const float sigma, const CImg<float>& Gkernel) {

	/* initialize kernel */
	const int M = Gkernel.size()/2;
	CImg<float> kernel(2*M+1);

	/* constant */
	const float inv_s2 = 1.F/(sigma*sigma);

	/* fill mask */
#pragma omp parallel for
	for(int i=0; i<=M; ++i) {
		const int i_over_s2 = i*inv_s2;
		const float value = -i_over_s2*(3*inv_s2+i_over_s2*i_over_s2)*Gkernel[M-i];
		kernel[M-i] = value;
		kernel[M+i] = value;
	}

	return kernel;
}

template<>
CImg<float> gaussian_kernel<3>(const float sigma) {
	return gaussian_kernel<3>(sigma, gaussian_kernel<0>(sigma));
}

} // namespace sampled_filter

