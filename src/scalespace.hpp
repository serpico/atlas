/*
 * scalespace.hpp
 *
 *  Created on: Apr 9, 2014
 *      Author: Antoine Basset
 * 
 * Inria Rennes - Bretagne Atlantique
 * Serpico
 * http://serpico.rennes.inria.fr
 */

#ifndef SCALESPACE_HPP_
#define SCALESPACE_HPP_

#include "CImg.h"
#include "fastcimg.hpp"
#include "imgFilter.hpp"


namespace scalespace {

/**
 * Scale-space representation.
 */
struct ss_rep {

	/* scale range */
	float tmin, tratio;
	unsigned tcount;
	float tmax;

	/* scale list */
	cimg_library::CImg<float> t;

	/* scale-space representation */
	cimg_library::CImgList<float> L;

	/**
	 * Evaluate scale range from tmin and tcount.
	 */
	float eval_scales(void) {
		t[0] = tmin;
		tmax = tmin;
		for(unsigned i=1; i<tcount; i++) {
			tmax *= tratio;
			t[i] = tmax;
		}
		return tmax;
	}

	/**
	 * Evaluate scale-space representation.
	 */
	template<typename T>
	bool eval_ss_rep(const cimg_library::CImg<T>& img) {
		using namespace cimg_library;

		if(tcount==0)
			return false;

#pragma omp parallel for
		cimglist_for(L, l) {
			CImg<float>& Ll = L[l];
			const CImg<T> blur = discrete_gaussian::get_blur<T, true>(img, sqrt(t[l]), 4);
			Ll.assign(img.width(), img.height(), img.depth(), img.spectrum());
			float *ptr = Ll.data();
			CImg_3x3(I, float);
			cimg_forZC(Ll, z, c)
				cimg_for3x3(blur, x, y, z, c, I, float)
					*ptr++ = Inc + Ipc + Icn + Icp - 4*Icc; //TODO separate directions
		}
		return true;
	}

	/**
	 * Build scale-space representation.
	 * \param img the image to represent
	 * \param tmin the minimum scale
	 * \param tratio the ratio between consecutive scales
	 * \param tcount the number of scales to compute
	 */
	template<typename T>
	ss_rep(
			const cimg_library::CImg<T>& img,
			float tmin=1./1.2, float tratio=1.2, unsigned tcount=10) :
			tmin(tmin), tratio(tratio), tcount(tcount),
			tmax(0),
			t(tcount), L(tcount) {
		eval_scales();
		eval_ss_rep(img);
	}
};

/**
 * Ordering for extrema (min or max).
 */
template<bool max>
bool is_better_than(float val, float ref) {
	return max?val>ref:val<ref;
}

/**
 * Detect local extrema in image.
 * \tparam max detect maxima or minima
 * \param img the objective function
 * \param radius neighborhood radius
 * \param support search domain
 */
template<bool max>
cimg_library::CImg<int> detect_extrema(
		const cimg_library::CImg<float>& img, int radius=1, int border=0,
		const cimg_library::CImg<bool>& support=cimg_library::CImg<bool>::empty()) {
	using namespace cimg_library;

	/* define support */
	def_dimensions(img, i);
	CImg<bool> search(i_w, i_h, i_d, i_s);
	if(support)
		search = support;
	else
		search.fill(true);

	/* find 2D extrema */
	CImgList<int> extrema;
	const int inset = std::max(radius, border);
	for(int z=0; z<i_d; z++) for(int c=0; c<i_s; c++) {

		/* loop over support */
		for(int y=inset; y<i_h-inset; y++) for(int x=inset; x<i_w-inset; x++) {
			bool& searchc = search(x, y, z, c, i_wh, i_whd);
			if(searchc) {
				const float& center = img(x, y, z, c, i_wh, i_whd);

				/* test neighborhood */
				for(int j=y-radius; j<=y+radius; j++) for(int i=x-radius; i<=x+radius; i++) {
					if(j!=y || i!=x) {
						const float& neighbor = img(i, j, z, c, i_wh, i_whd);
						bool& searchn = search(i, j, z, c, i_wh, i_whd);
						if(is_better_than<max>(center, neighbor))
							searchn = false;
						else
							searchc = false;
					}
				}

				/* add extremum */
				if(searchc)
					extrema.push_back(CImg<int>(1, 4, 1, 1, x, y, z, c));
			}
		}
	}

#ifdef DEBUG_MODE
	/* save positions */
	if(search.depth()==1 && search.spectrum()==1) {
		cimg_for_borderXY(search, x, y, inset)
			search(x, y) = false;
		search.save("search.tif", border);
	}
#endif

	return extrema.get_append('x');
}

/**
 * Detect and count extrema in scale-space.
 */
template<bool max>
cimg_library::CImgList<int> detect_blobs(
		const ss_rep& rep,
		const cimg_library::CImg<bool>& support=cimg_library::CImg<bool>::empty(),
		cimg_library::CImg<float>& scores=cimg_library::CImg<float>::empty(),
		int min_score=0) {

	/* get dimensions */
	const unsigned count = rep.tcount;
	if(count<3)
		return CImgList<int>::empty();
	const CImg<float>& L0 = rep.L[0];
	def_dimensions(L0, l);
	CImgList<int> blobs(count);
	blobs[0].assign(l_w, l_h, l_d, l_s, 0);
	blobs[count-2].assign(l_w, l_h, l_d, l_s, 0);
	if(scores)
		scores.assign(count, 1, 1, 1, 0);

	/* detect 2D-extrema */
#pragma omp parallel for
	for(unsigned t=1; t<count-1; t++)
		blobs[t] = detect_extrema<max>(rep.L[t], 1, int(sqrt(rep.t[t])+.5), support);

	/* detect blobs */
//#pragma omp parallel for
	for(unsigned t=1; t<count-1; t++) {
		CImg<int>& blobt = blobs[t];
		const CImg<float>& Ltm1 = rep.L[t-1];
		const CImg<float>& Lt = rep.L[t];
		const CImg<float>& Ltp1 = rep.L[t+1];

		/* loop over 2D extrema */
		cimg_forX(blobt, b) {
			const int x = blobt(b, 0);
			const int y = blobt(b, 1);
			const int z = blobt(b, 2);
			const int c = blobt(b, 3);
			const float val = Lt(x, y, z, c, l_wh, l_whd);

			/* test neighborhood */
			bool extreme = true;
			const int radius = 1;
			for(int j=y-radius; j<=y+radius; j++) {
				for(int i=x-radius; i<=x+radius; i++) {
					if(extreme)
						extreme =
								is_better_than<max>(val, Ltm1(i, j, z, c, l_wh, l_whd)) &&
								is_better_than<max>(val, Ltp1(i, j, z, c, l_wh, l_whd));
				}
			}

			if(extreme)
				if(scores)
					scores[t]++; //TODO register blob
		}
		const int inset = sqrt(rep.t[t])+.5;
		const unsigned area = (l_w-2*inset)*(l_h-2*inset)*l_d*l_s;
		if(scores[t]>min_score)
			scores[t] /= area;
		else
			scores[t] = -1;
	}

	return blobs;
}

cimg_library::CImgList<unsigned char> draw_blobs(
		const cimg_library::CImg<float>& img,
		const cimg_library::CImgList<int>& blobs);

} // namespace scalespace

#endif /* SCALESPACE_HPP_ */
