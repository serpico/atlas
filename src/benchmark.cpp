/*
 * main.cpp
 *
 *  Created on: Apr 11, 2014
 *      Author: Antoine Basset
 * 
 * Inria Rennes - Bretagne Atlantique
 * Serpico
 * http://serpico.rennes.inria.fr
 */

#include "core.hpp"
#include "evaluation.hpp"
#include "optionChecker.hpp"

bool VERBOSE;


int main(int argc, char **argv) {
	using namespace cimg_library;
	using namespace scalespace;
	using namespace atlas;
	TIFFSetWarningHandler(0);

	/* computation options */
#ifdef cimg_use_openmp
	omp_set_nested(0);
	opt_chk::read_thread_nb(argc, argv, false);
#endif

#ifdef DEBUG_MODE
	VERBOSE = true;
#else
	VERBOSE = cimg_option("-v", false, "Verbose mode");
#endif

	/* sequence options */
	const char *in_url = cimg_option("-i", (char *)0, "Input image");
	const int first = cimg_option("-f", 0, "First frame");
	const int last = cimg_option("-l", -1, "Last frame");
	const int step = cimg_option("-s", 1, "Frames step");
	const float x0 = cimg_option("-x0", 0.F, "Left margin (<0 for proportion of width)");
	const float y0 = cimg_option("-y0", 0.F, "Top margin (<0 for proportion of height)");
	const float x1 = cimg_option("-x1", -1.F, "Right margin (<0 for proportion of width)");
	const float y1 = cimg_option("-y1", -1.F, "Bottom margin (<0 for proportion of height)");

	/* detection options */
	const bool stabilize = cimg_option("-stab", false, "Stabilize CCD noise (GAT)"); //TODO
	const int t_slices = cimg_option("-slices", 10, "Number of slices for blob detection");
	const int score_min = cimg_option("-scoremin", 50, "Minimum number of blobs to consider scale");
	const float t_min = cimg_option("-tmin", 1.F, "Minimum scale"); //TODO from filename
	const float t_ratio = cimg_option("-tratio", 1.2, "Scale ratio");
	const int t_count = cimg_option("-tcount", 20, "Number of scales (if <3, tmin is used as best scale)");
	const char *t_url = cimg_option("-blob", "blobs.txt", "Reference blobs detection");
	const int rad_m = cimg_option("-radm", 10, "Min background distribution estimation radius");
	const int rad_M = cimg_option("-radM", 20, "Max background distribution estimation radius");
	const int rad_s = cimg_option("-rads", 5, "Step between background distribution estimation radiuses");
	const float pval_m = cimg_option("-pvalm", 1e-3, "Min vesicles detection Gaussian p-value");
	const float pval_M = cimg_option("-pvalM", 1e-1, "Max vesicles detection Gaussian p-value");
	const float pval_s = cimg_option("-pvals", pow(10, 0.2), "Ratio between vesicles detection Gaussian p-values");

	/* output options */
	const char *out_url = cimg_option("-o", "segmentation.tif", "Output files");

	/* evaluation options */
	const char *gt_url = cimg_option("-gt", (char*)0, "Ground-truth file");
	const char *mask_url = cimg_option("-mask", (char *)0, "Map of unconsidered points");
	const float tolerance = cimg_option("-tol", 4.F, "Allowed localization error");
	const char *res_url = cimg_option("-res", (char*)0, "Evaluation results file");

	/* help mode */
	if(opt_chk::check_help(argc, argv))
		return EXIT_SUCCESS;

	/* read sequence */
	CImg<float> sequence;
	sequence.load_tiff(in_url, first, last, step); //TODO other formats
	const int width = sequence.width();
	const int height = sequence.height();
	const int length = sequence.depth();
	int tslices;
	if(t_slices<0 || t_slices>length)
		tslices = length;
	else
		tslices = t_slices;
	sequence.crop(
			x0>=0?x0:-x0*width, y0>=0?y0:-y0*height,
			x1>=0?x1:-x1*width-1, y1>=0?y1:-y1*height-1);

	/* stabilize variance */
	if(stabilize)
		GAT::stabilize_variance(sequence);

	/* build scale-space representation */
	CImg<float> ref_blobs;
	try {
		ref_blobs.load(t_url);
	} catch(exception& e) {
		cout << "COUNTING BLOBS IN EPSILON" << endl;
		//TODO from filename
		const ss_rep rep(sequence.get_slices(0, tslices-1), t_min/t_ratio, t_ratio, t_count);
		CImg<float> scores(1);
		detect_blobs<false>(rep, CImg<bool>::empty(), scores);

		ref_blobs.assign(scores.width(), 2);
		cimg_foroff(scores, t) {
			ref_blobs(t, 0) = rep.t[t];
			ref_blobs(t, 1) = scores[t];
		}
		ref_blobs.save(t_url);
		return EXIT_SUCCESS;
	}
	const float tmin = ref_blobs(0, 0);

	/* select scale */
	const float scale = t_count>=3?select_scale(sequence.get_slices(0, tslices-1), ref_blobs, score_min):t_min;
	cout << "Best t\t" << scale << endl;

	/* enhance objects */
	const CImg<float> LoG = enhance_objects(sequence, scale);

	/* threshold */
	CImgList<float> index;
	CImgList<float> ROC;
	unsigned  i = 0;
	for(float r=rad_m; r<=rad_M+.5*rad_s; r+=rad_s) {
		for(float p=pval_m; p<=pval_M*sqrt(pval_s); p*=pval_s) {
			CImg<bool> segmentation = threshold_gaussian(LoG, p, r);
//			segmentation = discard_small_components<bool>(segmentation, 1); //TODO use?
			segmentation.save(out_url, i+1);
			CImg<float> params(2, 1, 1, 1, r, p);
			index.push_back(params);

			if(res_url) {
				CImgList<float> detections(segmentation.depth());
				cimg_forZ(segmentation, t)
					detections[t] = mask2centroids(segmentation.get_slice(t));
				CImgList<float> gt = read_centroids(gt_url); //TODO outside loop to save read time
				if(mask_url) {
					discard_centroids(detections, CImg<bool>(mask_url));
					discard_centroids(gt, CImg<bool>(mask_url));
				}
				CImg<float> results = eval_2Dt(detections, gt, tolerance);
				results.append(params);
				ROC.push_back(results);
			}

			i++;
		}
	}
	if(res_url)
		ROC.get_append('y').save(res_url);

	cout << "Output header:" << endl;
	cout << "N_OBJECTS\tN_DETECTIONS\t#TP\t#FP\t#FN\tTPR\tFPR\tPrecision\tRecall\tF-score\tnu\tp-val" << endl;

	return EXIT_SUCCESS;
}
