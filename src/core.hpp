/*
 * core.hpp
 *
 *  Created on: Apr 9, 2014
 *      Author: Antoine Basset
 * 
 * Inria Rennes - Bretagne Atlantique
 * Serpico
 * http://serpico.rennes.inria.fr
 */

#ifndef CORE_HPP_
#define CORE_HPP_

#include "CImg.h"
#include "fastcimg.hpp"
#include "scalespace.hpp"
#include "imgFilter.hpp"



/**
 * SLT-LoG detector as descibed in:
 * A. Basset, J. Boulanger, P. Bouthemy, J. Salamero, C. Kervrann.
 * SLT-LoG: A vesicle segmentation method with automatic scale selection
 * and local thresholding applied to TIRF microscopy.
 * ISBI'14
 */
namespace atlas {

/**
 * Select best scale according to criterion C2 (FPR/TPR minimization).
 * \param rep scale-space representation
 */
float select_scale(
		const cimg_library::CImg<float>& seq,
		const cimg_library::CImg<float>& ref_scores,
		bool verbose=false);

/**
 * Convolve image with LoG.
 * \param img the image of objects to enhance
 * \param scale the objects scale
 */
cimg_library::CImg<float> enhance_objects(const cimg_library::CImg<float>& img, float scale);

/**
 * Threshold map (locally or not) according to a p-value (supposing noise is Gaussian).
 * \param img the image to threshold
 * \param pval the p-value (of PFA)
 * \sigma the neighborhood size (Gaussian stdev); 0 for global threshold
 * \warning Keeps smallest values.
 */
cimg_library::CImg<bool> threshold_gaussian(
		const cimg_library::CImg<float>& img,
		float pval,
		float sigma=0);

/**
 * Remove smallest connected components.
 * \param prelim_map boolean presence map with small components
 * \param min_area the minimum acceptable component area
 * \paramt the return type (if not bool, return labeled map)
 */
template<typename T>
cimg_library::CImg<T> discard_small_components(const cimg_library::CImg<bool>& prelim_map, float min_area);


} // namespace atlas

template<typename T>
cimg_library::CImg<T> atlas::discard_small_components(
		const cimg_library::CImg<bool>& prelim_map, float min_area) {
	using namespace cimg_library;

	CImgList<bool> result = prelim_map.get_split('z');

#pragma omp parallel for
	cimg_forZ(prelim_map, z) {
		CImg<bool>& res = result[z];
		const CImg<unsigned> labels = res.get_label(false);
		const unsigned size = labels.max();
		if(size) {
			CImg<int> surfaces(size, 1, 1, 1, 0);

			cimg_forXY(labels, x, y) {
				const int i = labels(x, y)-1;
				if(i!=-1)
					surfaces(i)++;
			}

			cimg_forXY(res, x, y) {
				const int i = labels(x, y)-1;
				if(i!=-1 && surfaces(i)<=min_area)
					res(x, y) = false;
			}
		}
	}
	return result.get_append('z');
}

#endif /* CORE_HPP_ */
