/*

 Copyright (c) 1995-2005 by INRIA.
 All Rights Reserved.

 This software was developed at:
 IRISA/INRIA Rennes
 Campus Universitaire de Beaulieu
 35042 Rennes Cedex

 http://www.irisa.fr

 */

#include "evaluation.hpp"

using namespace std;
using namespace cimg_library;


/**
 * Evaluate performance measures.
 */
void eval_measures(CImg<float>& measures) {

	const unsigned objects = measures[N_OBJECTS];
	const unsigned detections = measures[N_DETECTIONS];
	measures[TPR] = measures[N_TP]/objects;
	measures[FPR] = measures[N_FP]/objects;
	measures[PRECISION] = measures[N_TP]/detections;
	measures[RECALL] = measures[N_TP]/objects;
	measures[F_SCORE] = 2.F*measures[PRECISION]*measures[RECALL]/(measures[PRECISION]+measures[RECALL]);
}


/**
 * Print performance measures.
 */
void print_measures(const CImg<float>& measures) {

	cout << "#objects\t" << measures[N_OBJECTS] << endl;
	cout << "#detections\t" << measures[N_DETECTIONS] << endl;
	cout << "#TP\t" << measures[N_TP] << endl;
	cout << "#FP\t" << measures[N_FP] << endl;
	cout << "#FN\t" << measures[N_FN] << endl;
	cout << "TPR\t" << measures[TPR] << endl;
	cout << "FPR*\t" << measures[FPR] << endl;
	cout << "precision\t" << measures[PRECISION] << endl;
	cout << "recall\t" << measures[RECALL] << endl;
	cout << "F-score\t" << measures[F_SCORE] << endl;
}


/**
 * Converts a detection binary mask to the centroids list.
 */
CImg<float> mask2centroids(const CImg<bool>& mask) {

	/* label mask */
	const CImg<unsigned>& labels = mask.get_label(false);
	const unsigned count = labels.max();
	if(!count)
		return CImg<float>::empty();

	/* find background label */
	unsigned bg = 0;
	while(mask[bg]>0)
		bg++;

	/* eval centroids */
	CImg<float> centroids(count, 2, 1, 1, 0.F);
	CImg<unsigned> areas(count, 1, 1, 1, 0U);

	cimg_forXY(labels, x, y) {

		const unsigned l = labels(x, y);
		if(l!=bg) {
			const unsigned idx = l<bg?l:l-1;
			centroids(idx, 0) += x;
			centroids(idx, 1) += y;
			areas[idx]++;
		}
	}
	cimg_foroff(areas, idx) {
		centroids(idx, 0) /= areas[idx];
		centroids(idx, 1) /= areas[idx];
	}
	return centroids;
}


/**
 * Read a (t,x,y)-table.
 */
CImgList<float> read_txy_table(const char *url) {

	CImgList<float> coords;				//< list of coordinates
	float t, x, y;						//< current coordinates
	CImg<float> point(1, 2, 1, 1);		//< current point
	fstream file(url, ios_base::in);	//< file stream

	/* read file */
	while(file >> t >> x >> y) {
		const unsigned frame = t+.5; // precision

		/* assign enough frames */
		while(coords.size()<=frame)
			coords.push_back(CImg<float>::empty());

		/* get point */
		point[0] = x;
		point[1] = y;
		coords[frame].append(point);
	}
	file.close();

	return coords;
}

/**
 * Read a INR file, where:
 * - x is the index of the track
 * - y is the index of the frame
 * - z=0 is the x-coordinate
 * - z=1 is the y-coordinate
 * - optionally, z=2 is the amplitude
 */
CImgList<float> read_inr(const char *url) {

	const CImg<float> inr(url);				//< input INR file
	CImgList<float> coords(inr.height());	//< list of coordinates
	CImg<float> point(1, 2, 1, 1);			//< current point

	/* read file */
	cimg_forY(inr, t) {

		/* get point */
		cimg_forX(inr, i) if(inr(i, t, 0)>0) {
			point[0] = inr(i, t, 0);
			point[1] = inr(i, t, 1);
			coords[t].append(point);
		}
	}
	return coords;
}


/**
 * Distance between two points.
 */
float distance(float xA, float yA, float xB, float yB) {

	const float delta_x = xB - xA;
	const float delta_y = yB - yA;
	return sqrt(delta_x*delta_x + delta_y*delta_y);
}


/**
 * Distance between points in lists.
 */
float distance(const CImg<float>& A, const CImg<float>& B, unsigned a, unsigned b) {

	const float xA = A(a, 0);
	const float yA = A(a, 1);
	const float xB = B(b, 0);
	const float yB = B(b, 1);
	return distance(xA, yA, xB, yB);
}


/**
 * Distance matrix between point lists.
 */
CImg<float> distance_matrix(const CImg<float>& A, const CImg<float>& B) {

	const unsigned sizeA = A.width();
	const unsigned sizeB = B.width();
	CImg<float> matrix(sizeA, sizeB);

	/* eval distances */
	for(unsigned a=0; a<sizeA; a++)
		for(unsigned b=0; b<sizeB; b++)
			matrix(a, b) = distance(A, B, a, b);
#ifdef DEBUG_MODE
	matrix.save("matrix.tif");
#endif
	return matrix;
}


/**
 * Find nearest neighbors in distance matrix.
 */
CImgList<unsigned> nearest_neighbors(const CImg<float>& matrix) {

	const unsigned sizeA = matrix.width();
	const unsigned sizeB = matrix.height();
	const float max = sizeA*sizeB>0?matrix.max()+1:1;
	CImg<unsigned> minA(sizeA, 1, 1, 1, max);
	CImg<unsigned> argminA(sizeA, 1, 1, 1, ~0U);
	CImg<unsigned> minB(sizeB, 1, 1, 1, max);
	CImg<unsigned> argminB(sizeB, 1, 1, 1, ~0U);

	/* find nearest neighbors */
	for(unsigned a=0; a<sizeA; a++) {
		for(unsigned b=0; b<sizeB; b++) {

			const float distance = matrix(a, b);
			if(distance<minA[a]) {
				minA[a] = distance;
				argminA[a] = b;
			}
			if(distance<minB[b]) {
				minB[b] = distance;
				argminB[b] = a;
			}
		}
	}
#ifdef DEBUG_MODE
	argminA.save("argminA.txt");
	argminB.save("argminB.txt");
#endif
	return CImgList<unsigned>(argminA, argminB);
}


/**
 * Eval performances for one 2D frame.
 * \param detections detections list
 * \param gt ground truth
 * \param tolerance allowed error
 */
CImg<float> eval_2D(const CImg<float>& detections, const CImg<float>& gt, float tolerance=1) {

	const CImg<float> matrix = distance_matrix(detections, gt);
	const unsigned sizeD = detections.width();	//< number of detections
	const unsigned sizeO = gt.width();			//< number of objects
	const CImgList<unsigned> neighbor = nearest_neighbors(matrix);
	const CImg<unsigned>& nnD = neighbor[0];	//< nearest neighbors from detections
	const CImg<unsigned>& nnO = neighbor[1];	//< nearest neighbors from objects

	/* init measures */
	CImg<float> measures(MEASURES_COUNT);
	measures[N_OBJECTS] = sizeO;
	measures[N_DETECTIONS] = sizeD;
	measures[N_TP] = 0;
	measures[N_FN] = sizeO;
	measures[N_FP] = sizeD;

	/* eval measures */
	for(unsigned d=0; d<sizeD; d++) {

		const unsigned nnd = nnD[d];
		const bool correspond = (nnO[nnd] == d);
		if(correspond && matrix(d, nnd)<tolerance) {
			measures[N_TP]++;
			measures[N_FP]--;
			measures[N_FN]--;
		}
	}
	eval_measures(measures);
	return measures;
}

cimg_library::CImgList<float> read_centroids(const char* url) {

	const char *ext = strrchr(url, '.') + 1;
	if(!ext || ext == url)	// no dot or hidden file dot
		return CImgList<float>::empty();

	CImgList<float> coords;

	/* TIFF file */
	if(!strcasecmp(ext, "tif") || !strcasecmp(ext, "tiff")) {
		const CImgList<bool> mask(url);
		cimglist_for(mask, t)
			coords.push_back(mask2centroids(mask[t]));
		return coords;
	}

	/* INR file */
	if(!strcasecmp(ext, "inr"))
		return read_inr(url);

	/* CSV file */
	if(!strcasecmp(ext, "txt") || !strcasecmp(ext, "csv") || !strcasecmp(ext, "prn"))
		return read_txy_table(url);

	return CImgList<float>::empty();
}

void discard_centroids(
		cimg_library::CImgList<float>& centroids,
		const cimg_library::CImg<bool>& mask) {

	cimglist_for(centroids, t) {
		CImg<float>& current = centroids[t];
		CImgList<float> discarded = current.get_split('x');
		for(int i=discarded.size()-1; i>=0; i--) {
			const int x = current(i, 0);
			const int y = current(i, 1);
			bool is_in_mask = false;
			if(mask(x, y))
				is_in_mask = true;
			else if(mask(x, y+1))
				is_in_mask = true;
			else if(mask(x+1, y))
				is_in_mask = true;
			else if(mask(x+1, y+1))
				is_in_mask = true;
			if(is_in_mask)
				discarded.remove(i);
		}
		current = discarded.get_append('x');
	}
}

/**
 * Eval overall performances.
 * \param detections detection list
 * \param gt ground-truth
 * \param tolerance allowed error
 */
CImg<float> eval_2Dt(const CImgList<float>& detections, const CImgList<float>& gt, float tolerance) {

	const unsigned lengthD = detections.size();
	const unsigned lengthG = gt.size();
	const unsigned length = cimg::min(lengthD, lengthG);
	CImg<float> measures(MEASURES_COUNT, 1, 1, 1, 0.F);

	/* sum performances over frames */
	for(unsigned t=0; t<length; t++)
		measures += eval_2D(detections[t], gt[t], tolerance);
	eval_measures(measures);
	return measures;
}


int main2(int argc, char **argv) {

	TIFFSetWarningHandler(0);

	/* arguments */
#ifdef DEBUG_MODE
	cout << "\nParsing command line..." << endl;
#endif
	const char *in_url = cimg_option("-i", (char *)0, "Input detection");
	const char *gt_url = cimg_option("-gt", (char *)0, "Ground truth");
	const char *out_url = cimg_option("-o", "measures.txt", "Output files");
	const int radius = cimg_option("-rad", 0, "Tolerance");

	/* get sequence */
#ifdef DEBUG_MODE
	cout << "\nReading detection file:\n\t" << in_url << endl;
#endif
	CImgList<float> detections(in_url);
	if(detections[0].width()!=2)
		cimglist_for(detections, t)
			detections[t] = mask2centroids(detections[t]);

	/* get ground-truth */
#ifdef DEBUG_MODE
	cout << "\nReading ground-truth file:\n\t" << gt_url << endl;
#endif
	CImgList<float> gt = read_txy_table(gt_url);

	/* eval performances */
#ifdef DEBUG_MODE
	cout << "\nEvaluating performances..." << endl;
#endif
	const CImg<float> measures = eval_2Dt(detections, gt, radius);
	measures.save(out_url);
	print_measures(measures);
#ifdef DEBUG_MODE
	cout << "\nResults saved as:\n\t" << out_url << endl;
#endif

	return EXIT_SUCCESS;
}
