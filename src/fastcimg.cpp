/*
 * fastcimg.cpp
 *
 *  Created on: Apr 11, 2014
 *      Author: Antoine Basset
 * 
 * Inria Rennes - Bretagne Atlantique
 * Serpico
 * http://serpico.rennes.inria.fr
 */


#include "fastcimg.hpp"


double approx::gauss_cdf(double x) {

	/* constants */
	const double a1 =  0.254829592;
	const double a2 = -0.284496736;
	const double a3 =  1.421413741;
	const double a4 = -1.453152027;
	const double a5 =  1.061405429;
	const double p  =  0.3275911;

	/* computation */
	const int sign = x<0?-1:1;
	const double u = fabs(x)/sqrt(2.0);
	const double t = 1./(1.0 + p*u);
	const double y = 1. - (((((a5*t + a4)*t) + a3)*t + a2)*t + a1)*t*exp(-u*u);

	return .5*(y*sign+1);
}

double approx::gauss_cdf(double x, double mean, double stdev) {
	return gauss_cdf(x*stdev+mean);
}

double approx::gauss_l_quantile(double p) {

	/* constants */
	const double c0 = 2.515517;
	const double c1 = 0.802853;
	const double c2 = 0.010328;
	const double d1 = 1.432788;
	const double d2 = 0.189269;
	const double d3 = 0.001308;

	/* computation */
	const bool tail = p<=0.5;
	const double u = tail?p:1.-p;
	const double t2 = -log(u*u);
	const double t = sqrt(t2);
	const double t3 = t*t2;
	const double c = c0 + c1*t + c2*t2;
	const double d = 1 + d1*t + d2*t2 + d3*t3;

	return tail?c/d-t:t-c/d;
}

double approx::gauss_r_quantile(double p) {
	return gauss_l_quantile(1.-p);
}

double approx::gauss_l_quantile(double p, double mean, double stdev) {
	return mean+stdev*gauss_l_quantile(p);
}

double approx::gauss_r_quantile(double p, double mean, double stdev) {
	return mean+stdev*gauss_r_quantile(p);
}

