/*
 * main.cpp
 *
 *  Created on: Apr 11, 2014
 *      Author: Antoine Basset
 * 
 * Inria Rennes - Bretagne Atlantique
 * Serpico
 * http://serpico.rennes.inria.fr
 */

#include "evaluation.hpp"
#include "optionChecker.hpp"

bool VERBOSE;


int main(int argc, char **argv) {
	using namespace cimg_library;
	TIFFSetWarningHandler(0);

	/* computation options */
#ifdef cimg_use_openmp
	omp_set_nested(0);
	opt_chk::read_thread_nb(argc, argv, false);
#endif

#ifdef DEBUG_MODE
	VERBOSE = true;
#else
	VERBOSE = cimg_option("-v", false, "Verbose mode");
#endif

	/* sequence options */
	const char *in_url = cimg_option("-i", (char *)0, "Detection file");
	const char *gt_url = cimg_option("-gt", (char*)0, "Ground-truth file");
	const char *res_url = cimg_option("-res", (char*)0, "Evaluation results file");
	const float tolerance = cimg_option("-tol", 4.F, "Allowed localization error");

	/* help mode */
	if(opt_chk::check_help(argc, argv))
		return EXIT_SUCCESS;

	const CImgList<float> detections = read_centroids(in_url);
	const CImgList<float> gt = read_centroids(gt_url);
	gt.save("gt.tif");
	unsigned min=~0U, max=0;
	cimglist_for(gt, t) {
		unsigned n = gt[t].size()/2;
		if(n<min)
			min=n;
		if(n>max)
			max=n;
	}
	cout << min << '\t' << max << endl;
	CImg<float> results = eval_2Dt(detections, gt, tolerance);

	if(!res_url)
		cout << "Output header:" << endl;
	cout << "N_OBJECTS\tN_DETECTIONS\t#TP\t#FP\t#FN\tTPR\tFPR\tPrecision\tRecall\tF-score\tnu\tp-val" << endl;
	if(res_url)
		results.save(res_url);
	else
		cimg_foroff(results, i)
			cout << results[i] << '\t';
	cout << endl;

	return EXIT_SUCCESS;
}
