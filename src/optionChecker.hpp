/*
 * optionChecker.hpp
 *
 *  Created on: 2 avr. 2013
 *      Author: abasset
 */

#ifndef OPTIONCHECKER_HPP_
#define OPTIONCHECKER_HPP_

#include <stdio.h>
#include <stdlib.h>
#ifdef __SunOS_
# include <iostream.h>
#else
# include <iostream>
#endif

#include "CImg.h"

using namespace std;
using namespace cimg_library;

/**
 * A set of tools to check options or more generally variables.
 * - Check that a given variable lies inside a given interval.
 * - Check that a given variable differs from another.
 * - Check that the help option (all CImg variants) has been entered.
 * - Get the maximum number of useable threads.
 *
 * \warning Contains some deprecated functions I strongly recommend to avoid.
 */
namespace opt_chk {
    
    /**
     * Check that a value is included into an interval.
     * \param variable the name of the variable used to print error (0 for silent mode)
     * \param value the value of the variable
     * \param inf the inferior endpoint
     * \param min inferior endpoint included (inf is a min)
     * \param sup the superior endpoint
     * \param max superior bound included (sup is a max)
     */
    template<typename T>
    bool check_range(
                     const char *variable, const T value,
                     const T inf, const bool min,
                     const T sup, const bool max) {
        
        if(value>inf && value<sup)
            return true;
        if(min && value==inf)
            return true;
        if(max && value==sup)
            return true;
        
        if(!variable)
            return false;
        
        cerr << "ERROR: " << variable << " is ";
        cerr << value;
        cerr << " but should belong to ";
        if(min)
            cerr << "[";
        else
            cerr << "]";
        cerr << inf;
        cerr << ", ";
        cerr << sup;
        if(max)
            cerr << "]";
        else
            cerr << "[";
        cerr << "." << endl;
        
        return false;
    }
    
    /**
     * Check that a value is greater than a given bound.
     * \sa check_range(const char *variable, T value, T inf, bool min, T sup, bool max)
     * \sa check_sup(const char *variable, T value, T sup, bool max)
     */
    template<typename T>
    bool check_inf(
                   const char *variable, const T value,
                   const T inf, const bool min) {
        
        if(value>inf)
            return true;
        if(min && value==inf)
            return true;
        
        if(!variable)
            return false;
        
        cerr << "ERROR: " << variable << " is ";
        cerr << value;
        cerr << " but should be greater than ";
        if(min)
            cerr << "or equal to ";
        cerr << inf;
        cerr << "." << endl;
        
        return false;
    }
    
    /**
     * Check that a value is less than a given bound.
     * \sa check_range(const char *variable, T value, T inf, bool min, T sup, bool max)
     * \sa check_inf(const char *variable, T value, T inf, bool min)
     */
    template<typename T>
    bool check_sup(
                   const char *variable, const T value,
                   const T sup, const bool max) {
        
        if(value<sup)
            return true;
        if(max && value==sup)
            return true;
        
        if(!variable)
            return false;
        
        cerr << "ERROR: " << variable << " is ";
        cerr << value;
        cerr << " but should be less than ";
        if(max)
            cerr << "or equal to ";
        cerr << sup;
        cerr << "." << endl;
        
        return false;
    }
    
    /**
     * Check that a value is different than another forbidden one.
     * \param variable the name of the variable used to print error (0 for silent mode)
     * \param value the value of the variable
     * \param forbidden the forbidden value
     */
    template<typename T>
    bool check_inequal(
                       const char *variable, const T value,
                       const T forbidden) {
        
        if(value!=forbidden)
            return true;
        
        if(!variable)
            return false;
        
        cerr << "ERROR: " << variable << " must be different than ";
        cerr << forbidden;
        cerr << "." << endl;

        return false;
    }
    
    /**
     * Check if the help option has been entered.
     * \param argc the number of arguments
     * \param argv the list of arguments
     * \return \c true if \c -h, \c -help or \c --help have been entered
     */
    bool check_help(int argc, char **argv) {
        const char *is_h = cimg_option("-h",(char*)0,"Display help");
        const char *is_help = cimg_option("-help",(char*)0,0);
        const char *is__help = cimg_option("--help",(char*)0,0);
        return is_h || is_help || is__help;
    }
    
//    /**
//     * Read a configuration file.
//     * \param conf_url the URL of the configuration file
//     * \param argc the number of arguments
//     * \param argv the list of arguments
//     * \warning DEPRECATED: DO NOT USE ANYMORE (please)
//     */
//    void read_config(const char *conf_url, int& argc, char **argv) {
//
//        vector<string> options;
//        for(int a=0; a<argc; a++) {
//            string word(argv[a]);
//            options.push_back(word);
//        }
//        ifstream in(conf_url);
//        while(in) {
//            string word;
//            in >> word;
//            options.push_back(word);
//        }
//
//        //TODO realloc
//        argc = options.size();
//        for(int a=0; a<argc; a++)
//            argv[a] = strdup(options[a].c_str());
//    }
    
    /**
     * Get the max number of threads from command line.
     * \param argc the number of arguments
     * \param argv the arguments
     * \param verbose to print parsed value
     * \param parallel_default if \c true, the default value is the max number of hyperthreads supported; else parallel computation is disabled by default
     */
    int read_thread_nb(int argc, char **argv, bool verbose=true, bool parallel_default=false) {
      const int max_threads = 1; //omp_get_max_threads();
        const int thread_nb = cimg_option("-j", parallel_default?max_threads:1, "Number of threads for parallel computation");
        if(!check_range("Number of threads", thread_nb, 1, true, max_threads, true))
            return 0;
        //omp_set_num_threads(thread_nb);
        if(verbose && thread_nb>1)
            cout << "Parallel computing (" << thread_nb << " threads)" << endl;
        return thread_nb;
    }
    
} // namespace opt_chk

#endif /* OPTIONCHECKER_HPP_ */
