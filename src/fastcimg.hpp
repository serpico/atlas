/*
 * fastcimg.hpp
 *
 *  Created on: Mar 21, 2014
 *      Author: Antoine Basset
 */

#ifndef FASTCIMG_HPP_
#define FASTCIMG_HPP_

#include "CImg.h"

/**
 * Define dimensions of an image.
 * Six dimensions are defined:
 * - prefix_w, prefix_h, prefix_d, prefix_s are the respective width, height, depth and spectrum of img
 * - prefix_wh = width*height and prefix_whd = width*height*depth fasten access to image values through the () operator.
 */
#define def_dimensions(img, prefix) \
	const int prefix##_w = (img).width(); \
	const int prefix##_h = (img).height(); \
	const int prefix##_d = (img).depth(); \
	const int prefix##_s = (img).spectrum(); \
	const int prefix##_wh = prefix##_w*prefix##_h; \
	const int prefix##_whd = prefix##_wh*prefix##_d; \
	const int prefix##_size = prefix##_whd*prefix##_s; (void)prefix##_size; \

/**
 * Define statistics of an image:
 * - prefix_stats, corresponding to img.get_stats(), is a constant CImg<double>.
 * - prefix_min, prefix_max, prefix_mean, prefix_var, prefix_stdev are defined as constant doubles.
 * The variance estimation method can be chosen with the method parameter (default is 1 in CImg.h)
 */
#define def_statistics(img, prefix, method) \
	const CImg<double> prefix##_stats = (img).get_stats(method); \
	const double prefix##_min = prefix##_stats[0]; (void)prefix##_min; \
	const double prefix##_max = prefix##_stats[1]; (void)prefix##_max; \
	const double prefix##_mean = prefix##_stats[2]; (void)prefix##_mean; \
	const double prefix##_var = prefix##_stats[3]; (void)prefix##_var; \
	const double prefix##_stdev = std::sqrt(prefix##_var); (void)prefix##_stdev; \



namespace omp4cimg {

/**
 * Parallel evaluation of sum.
 */
template<typename T>
T par_sum(const cimg_library::CImg<T>& img) {

	using namespace cimg_library;

	T sum = T(0);
	const unsigned long size = img.size();

	T *begin = img._data;
	T *end = begin+size;
#pragma omp parallel for reduction(+:sum)
	for(T *ptrs=begin; ptrs<end; ptrs++)
		sum += *ptrs;

	return sum;
}

/**
 * Parallel evaluation of mean.
 */
template<typename T>
T par_mean(const cimg_library::CImg<T>& img) {

	using namespace cimg_library;

	T sum = T(0);
	const unsigned long size = img.size();

	T *begin = img._data;
	T *end = begin+size;
#pragma omp parallel for reduction(+:sum)
	for(T *ptrs=begin; ptrs<end; ptrs++)
		sum += *ptrs;

	return sum/size;
}

/**
 * Parallel evaluation of moment.
 */
template<typename T>
T par_moment(const cimg_library::CImg<T>& img, int order) {

	using namespace cimg_library;

	T sum = T(0);
	const unsigned long size = img.size();

	T *begin = img._data;
	T *end = begin+size;
#pragma omp parallel for reduction(+:sum)
	for(T *ptrs=begin; ptrs<end; ptrs++)
		sum += pow(*ptrs, order);

	return sum/size;
}

/**
 * Parallel evaluation of mean and variance.
 */
template<typename T>
T par_meanvar(const cimg_library::CImg<T>& img, T& mean, bool unbiaised=false) {

	using namespace cimg_library;

	T sum(0);
	T sum2(0);
	const unsigned long size = img.size();

	T *begin = img._data;
	T *end = begin+size;
#pragma omp parallel for reduction(+:sum,sum2)
	for(T *ptrs=begin; ptrs<end; ptrs++) {
		const T val = *ptrs;
		sum += val;
		sum2 += val*val;
	}

	mean = sum/size;
	return sum2/(size-unbiaised)-mean*mean;
}

/**
 * Parallel evaluation of variance.
 */
template<typename T>
T par_var(const cimg_library::CImg<T>& img, bool unbiaised=false) {
	T mean;
	return par_meanvar(img, mean, unbiaised);
}

template<typename T>
T par_gradZ_fwd(const cimg_library::CImg<T>& img) {

	cimg_library::CImg<T> grad(img);
	def_dimensions(grad, g);
	T *begin = grad.data();
	T *end = begin+g_size-g_wh;

#pragma omp parallel for
	for(T *ptrs=begin; ptrs<end; ptrs++)
		*ptrs = *(ptrs+g_wh) - *ptrs;

	return grad;
}

template<typename T>
T par_gradZ_bwd(const cimg_library::CImg<T>& img) {

	cimg_library::CImg<T> grad(img);
	def_dimensions(grad, g);
	T *begin = grad.data()+g_wh;
	T *end = begin+g_size;

#pragma omp parallel for
	for(T *ptrs=end-1; ptrs>=begin; ptrs--)
		*ptrs -= *(ptrs-g_wh);

	return grad;
}

} // namespace omp4cimg


namespace approx {

/**
 * Gaussian CDF.
 * From Hastings, Approximations for digital computers.
 * Error < 7.5e-8
 */
double gauss_cdf(double x);

/**
 * Gaussian CDF.
 */
double gauss_cdf(double x, double mean, double stdev);

/**
 * Left quantile of standard normal distribution.
 * From Hastings, Approximations for digital computers.
 * Error < 4.5e-4
 */
double gauss_l_quantile(double p);

/**
 * Right quantile of standard normal distribution.
 */
double gauss_r_quantile(double p);

/**
 * Left quantile of normal distribution.
 */
double gauss_l_quantile(double p, double mean, double stdev);

/**
 * Right quantile of normal distribution.
 */
double gauss_r_quantile(double p, double mean, double stdev);

} // namespace approx


#endif /* FASTCIMG_HPP_ */
