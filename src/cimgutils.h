/* -*- mode:c++ -*-
 *
 * \file cimgutils.h
 * \author Jerome Boulanger jerome.boulanger@curie.fr
 * \brief Collection of utility for CImg
 */

#pragma once
#ifndef _CIMGUTILS_H_
#define _CIMGUTILS_H_
//#include "CFigure.h"

#include "CImg.h"
using namespace cimg_library;


//! Some loops
#define cimg_for_step1(bound,i,step) for (int i = 0; i<(int)(bound); i+=step)
#define cimg_for_stepX(img,x,step) cimg_for_step1((img)._width,x,step)
#define cimg_for_stepY(img,y,step) cimg_for_step1((img)._height,y,step)
#define cimg_for_stepZ(img,z,step) cimg_for_step1((img)._depth,z,step)
#define cimg_for_stepXY(img,x,y,step) cimg_for_stepY(img,y,step) cimg_for_stepX(img,x,step)
#define cimg_for_stepXYZ(img,x,y,step) cimg_for_stepZ(img,z,step) cimg_for_stepY(img,y,step) cimg_for_stepX(img,x,step)

//! Loop for point J(xj,yj) in the neighborhood of a point I(xi,yi) of size (2*rx+1,2*ry+1)
/**
   Point J is kept inside the boundaries of the image img.
   example of summing the pixels values in a neighborhood 11x11
   cimg_forXY(img,xi,yi) cimg_for_windowXY(img,xi,yi,xj,yj,5,5) dest(yi,yi) += src(xj,yj);
 **/
#define cimg_forXY_window(img,xi,yi,xj,yj,rx,ry)			\
		for (int yi0=cimg::max(0,yi-ry), yi1=cimg::min(yi+ry,(int)img.height()-1), yj=yi0;yj<=yi1;++yj) \
		for (int xi0=cimg::max(0,xi-rx), xi1=cimg::min(xi+rx,(int)img.width()-1), xj=xi0;xj<=xi1;++xj)

//! Loop for two points (xi_,yi_) and (xj_,yj_) in the neighborhood of two point (xi,yi) and (xj,yj) of size (2*rx+1,2*ry+1)
// this is usefull for manupulating patches
#define cimg_forXY_patch(img,xi,yi,xj,yj,xi_,yi_,xj_,yj_,px,py)  \
		const int dxp=cimg::min(xi-cimg::max(xi-px,0),xj-cimg::max(xj-px,0));  \
		const int dxn=cimg::min(cimg::min(xi+px,(int)img.width()-1)-xi,cimg::min(xj+px,(int)img.width()-1)-xj); \
		const int dyp=cimg::min(yi-cimg::max(yi-py,0),yj-cimg::max(yj-py,0));	\
		const int dyn=cimg::min(cimg::min(yi+py,(int)img.height()-1)-yi,cimg::min(yj+py,(int)img.height()-1)-yj); \
		const int pxi0=xi-dxp, pxi1=xi+dxn, pyi0=yi-dyp, pyi1=yi+dyn;		\
		const int pxj0=xj-dxp, pxj1=xj+dxn, pyj0=yj-dyp, pyj1=yj+dyn;		\
		for (int yi_=pyi0,yj_=pyj0; yi_<=pyi1 && yj_<=pyj1; ++yi_, ++yj_)	\
		for (int xi_=pxi0,xj_=pxj0; xi_<=pxi1 && xj_<=pxj1; ++xi_, ++xj_)	\

#define cimg_forXYZ_window(img,xi,yi,zi,xj,yj,zj,rx,ry,rz)		                        \
		for (int zi0=cimg::max(0,zi-rz), zi1=cimg::min(zi+rz,(int)img.depth()-1) , zj=zi0;zj<=zi1;++zj) \
		for (int yi0=cimg::max(0,yi-ry), yi1=cimg::min(yi+ry,(int)img.height()-1), yj=yi0;yj<=yi1;++yj) \
		for (int xi0=cimg::max(0,xi-rx), xi1=cimg::min(xi+rx,(int)img.width()-1) , xj=xi0;xj<=xi1;++xj)

// loop for 2 patch of size p around (xi,yj) and (xj,yj)
#define cimg_forXYZ_patch(img,xi,yi,zi,xj,yj,zj,xi_,yi_,zi_,xj_,yj_,zj_,px,py,pz) \
		const int dxp=cimg::min(xi-cimg::max(xi-px,0),xj-cimg::max(xj-px,0));  \
		const int dxn=cimg::min(cimg::min(xi+px,(int)img.width()-1)-xi,cimg::min(xj+px,(int)img.width()-1)-xj); \
		const int dyp=cimg::min(yi-cimg::max(yi-py,0),yj-cimg::max(yj-py,0));	\
		const int dyn=cimg::min(cimg::min(yi+py,(int)img.height()-1)-yi,cimg::min(yj+py,(int)img.height()-1)-yj); \
		const int dzp=cimg::min(zi-cimg::max(zi-pz,0),zj-cimg::max(zj-pz,0));	\
		const int dzn=cimg::min(cimg::min(zi+pz,(int)img.depth()-1)-zi,cimg::min(zj+pz,(int)img.depth()-1)-zj); \
		const int pxi0=xi-dxp, pxi1=xi+dxn, pyi0=yi-dyp, pyi1=yi+dyn, pzi0=zi-dzp, pzi1=zi+dzn;		\
		const int pxj0=xj-dxp, pxj1=xj+dxn, pyj0=yj-dyp, pyj1=yj+dyn, pzj0=zj-dzp, pzj1=zj+dzn;		\
		for (int zi_=pzi0,zj_=pzj0; zi_<=pzi1 && zj_<=pzj1; ++zi_, ++zj_)	\
		for (int yi_=pyi0,yj_=pyj0; yi_<=pyi1 && yj_<=pyj1; ++yi_, ++yj_)	\
		for (int xi_=pxi0,xj_=pxj0; xi_<=pxi1 && xj_<=pxj1; ++xi_, ++xj_)	\

//! Compute the distance between two 2d points
inline float distance(const float xi, const float yi,
		const float xj, const float yj){
	const float dx = xi - xj, dy = yi - yj;
	return sqrt(dx * dx + dy * dy);
}

//! Compute the distance between two 3d points
inline float distance(const float xi, const float yi, const float zi,
		const float xj, const float yj, const float zj){
	const float dx = xi - xj, dy = yi - yj, dz = zi - zj;
	return sqrt(dx * dx + dy * dy + dz * dz);
}

inline float gaussian(const float x, const float y, const float s){
	const float d = (x - y) / s;
	return exp(-.5 * d * d);
}

inline float gaussian(const float xi, const float yi,
		const float xj, const float yj,
		const float sx, const float sy){
	const float dx = (xi - xj) / sx, dy = (yi - yj) / sy;
	return exp(-.5 * (dx * dx + dy * dy));
}

inline float gaussian(const float xi, const float yi, const float zi,
		const float xj, const float yj, const float zj,
		const float sx, const float sy, const float sz){
	const float dx = (xi - xj) / sx, dy = (yi - yj) / sy,  dz = (zi - zj) / sz;
	return exp(-.5 * (dx * dx + dy * dy + dz * dz));
}

//! Crop an image according to a string
template <typename T>
CImg<T> crop(const CImg<T> &img, const char *crop_str){
	if (img.is_empty()) return img;
	if (crop_str != 0) {
		int x0, y0, x1, y1;
		int n = sscanf(crop_str,"%d,%d,%d,%d",&x0,&y0,&x1,&y1);
		if (n==4){
			return img.get_crop(x0,y0,x1,y1);
		} else return img;
	} else {
		return img;
	}
}

//! add a label to filename
inline const char * filename_auto(const char * const filename,
		const char * const label){
	static char name[1024] = { 0 };
	char body[1024] = { 0 };
	const char *const ext = cimg::split_filename(filename, body);
	cimg_snprintf(name, sizeof(name), "%s%s.%s", body, label, ext);
	return name;
}

//! robust linear fitting y = a+ b x
/**
   \param x the X coordinates
   \param y the Y coordinates
   \param robust robust regression (M-estimation)
   \param print print information
 **/
template<typename T>
void linear_fit(const CImg<T> &x, const CImg<T> & y,
		double & a, double & b, const int robust=0, const bool print=false) {
	if (x.is_empty()) throw CImgException("linear_fit() in file %s at line %d: x is empty",__FILE__,__LINE__);
	if (y.is_empty()) throw CImgException("linear_fit() in file %s at line %d: y is empty",__FILE__,__LINE__);
	if (x.size()!=y.size()) throw CImgException("linear_fit() in file %s at line %d: CImg<>x(%d,%d,%d,%d) and"
			"CImg<>y(%d,%d,%d,%d) have not the same size",
			x.width(),x.height(),x.depth(),x.depth(),
			y.width(),y.height(),y.depth(),y.depth(),
			__FILE__,__LINE__);
	double sw = 0, swxx = 0, swx = 0,swy = 0, swxy = 0;
	// init least square
	const float *ptr_xi=x.data(), *ptr_yi=y.data();
	for (unsigned int i=0;i<x.size();++i) {
		const float xi = *ptr_xi++, yi = *ptr_yi++;
		sw++;
		swx += xi;     swy += yi;
		swxx += xi*xi; swxy += xi*yi;
	}
	double d = sw*swxx-swx*swx;
	if (cimg::abs(d)>1.0e-10) {
		a = (swxx*swy-swx*swxy)/d;
		b = (sw*swxy-swx*swy)/d;
	}else{
		a=0; b=1;
		cimg::warn(" %s line %d in linear_fit() : Dividing by %g.\n",__FILE__,__LINE__,d);
	}
	if (print)
		printf("   Fitting OLS :  Var[Y]= %.3f E[Y] +  %.3f \n",b,a);
	if (robust==1) {
		// compute variance of residuals
		double sigma = 0, mean = 0, nsamples = 0, r_new = 0, r_old = 0;
		ptr_xi = x.data(); ptr_yi = y.data();
		for (unsigned int i = 0; i < x.size(); ++i) {
			const double xi = *ptr_xi++, yi = *ptr_yi++, delta = yi-(a+b*xi);
			mean += delta; sigma += delta*delta; nsamples++;
		}
		mean /= nsamples; sigma /= nsamples; r_new=sigma; sigma -= mean*mean;

		if (print)
			printf("   Residuals : %.3g, sigma : %.3g, mean : %.3g\n",r_new,std::sqrt(sigma),mean);
		// IRLS
		unsigned int k=0;
		while ( k<1000 && (k<5 || cimg::abs((r_old-r_new)/r_old)>1e-12) )  {
			ptr_xi=x.data(); ptr_yi=y.data();
			r_old=r_new; r_new=0; sw=0; swxx=0; swx=0; swy=0; swxy=0;
			for (unsigned int i = 0; i < x.size(); ++i){
				const double xi = *ptr_xi++, yi = *ptr_yi++,
						delta = yi-(a+b*xi), w = std::exp(-0.5*delta*delta/(9.0*sigma));
				if (w==w){//if (!isnan(w)){
					sw += w;
					swx += w*xi;     swy += w*yi;
					swxx += w*xi*xi; swxy += w*xi*yi;
					r_new += delta*delta;
				}
			}
			d = sw * swxx - swx * swx; r_new /= nsamples;
			if (cimg::abs(d)>1/(cimg::type<double>::max()-1)) {
				a = (swxx*swy-swx*swxy)/d;
				b = (sw*swxy-swx*swy)/d;
			} else {
				cimg::warn("Warning in %s line %d in linear_fit() : Dividing by %g.",__FILE__,__LINE__,d);
			}
			++k;
		}
		if (print)
			printf("   Fitting IRLS(%d) : Var[Y]= %.3f E[Y] +  %.3f , R=%.3g\n",k,b,a,r_new);
	}
}

//! compute the Z-profile at position (x,y).
/**
   \param img the input 3D image
   \param x the x-coordinate
   \param y the y-coordinate
   \param radius is radius is >0 then a Gaussian average is taken around the point
 **/
template<typename T>
CImg<> zprofile(const CImg<T> &img , const float x, const float y, const float radius=0){
	CImg<> line(img.depth());
	cimg_forZ(img,z){
		float s = 0, sw = 0;
		cimg_forXY_window(img, x, y, xj, yj, 3 * radius, 3 * radius){
			const float w = radius==0?1:gaussian(x,y,xj,yj,radius,radius);
			s += w * img.linear_atXYZ(xj,yj,z);
			sw += w;
		}
		line(z) = s / sw;
	}
	return line;
}

//! return the full width half max of the signal
template<typename T>
float fwhm(const CImg<T> & line){
	float M = 0.5f * (line.max() - line.min());
	int z0 = 0, z1 = 0;
	cimg_forX(line, z){
		if (line(z) >= M && z0==0) z0 = z;
		if (line(line.width()-1-z) >= M && z1==0) z1 = line.width()-1-z;
	}
	if (z0 == 0 || z1 == 0) return -1.;
	float z0f = z0 + (M - line(z0)) / (line(z0 + 1)-line(z0));
	float z1f = z1 - (M - line(z1)) / (line(z1 - 1)-line(z1));
	return z1f-z0f;
}

//! compute the maximum intensity projection
template<typename T>
CImg<T> maximum_intensity_projection(const CImg<T> & img, const char axis='z'){
	CImg<> dest;
	if (axis=='z'){
		dest.assign(img.width(),img.height(),1,img.spectrum());
		cimg_forXYC(img,x,y,c){
			T maxi = cimg::type<T>::min();
			cimg_forZ(img,z){
				if (img(x,y,z,c) > maxi)  maxi = img(x,y,z,c);
			}
			dest(x,y,c) = maxi;
		}
	}
	return dest;
}


//! Generate a nx2 table representing the coordinates of non overlapping spots.
/**
   \param img the image
   \param number the number of spots to be drawn
   \param sigma the size of the spot
   \param hardcore true is spots do not overlap (overlapping defined by 3 sigma)
   The spots are drawn inside the the image
 **/
template<typename T>
CImg<> generate_random_coordinates(const CImg<T> &img, const int number, const float sigma=1.f, const bool hardcore=true){
	CImg<> dest(number, 2);
	for (int i = 0; i < number; i++){
		bool ok = true;
		int k = 0;
		float xi,yi;
		do {
			ok = true;
			xi = 3 * sigma + (img.width() - 6 * sigma) * cimg::rand();
			yi = 3 * sigma + (img.height() - 6 * sigma) * cimg::rand();
			if (hardcore) {
				float dmin = img.width();
				for (int j = 0; j < i; j++){
					dmin = cimg::min(dmin, distance(xi, yi, dest(j, 0), dest(j, 1)));
				}
				if (dmin > 3 * sigma) ok = true; else ok = false;
			}
			k++;
		} while (!ok && k < 10000);
		dest(i,0) = xi;
		dest(i,1) = yi;
	}
	return dest;
}

//! Generates n sample for the probability density function using rejection sampling.
/**
   \param img : input image of the dendity
   \param n : number of samples.

   \note This can be used to produce very low intensity images (with n
   photons) and get a Poisson distribution. The input image is
   normalized automatically to ensure the the sum of the probability
   density function is 1.

   This function can be accelerated by using OpenMP (cimg_use_openmp).
 **/
template<typename T>
CImg<T> rejection_sampling(const CImg<T> & img, const unsigned int n) {
	if (img.is_empty()) throw CImgException("The image is empty.");
	CImg<T> dest(img._width,img._height,img._depth,img._spectrum,0);
	CImg<float> pdf(img);
	pdf.normalize(0,1);
	const T s = pdf.sum();
	if (s!=0) { pdf /= s; }
	else { throw CImgException("get_samples_from_pdf: sum of input is 0"); }
	const float iMf = 1.0/pdf.max();
#ifdef cimg_use_openmp
#pragma omp parallel for
#endif
	for (unsigned int k = 0; k < n; ++k) {
		bool flag = true;
		while (flag) {
			unsigned int x = img._width * cimg::rand(), y = img._height * cimg::rand(),
					z = img._depth * cimg::rand(), c = img._spectrum * cimg::rand();
			if (pdf(x,y,z,c) * iMf > cimg::rand()) {
				flag = false;
				dest(x,y,z,c)++;
			}
		}
	}
	return dest;
}

//! Draw circles from the given nx2 list of coordinates and the size sigma
template<typename T,typename Tc>
void draw_circle_from_coordinates(CImg<T> &img, const CImg<Tc> &coordinates, const float sigma, const float color[3]){
	cimg_forX(coordinates, i){
		const int xi = cimg::round(coordinates(i, 0)), yi = cimg::round(coordinates(i, 1));
		img.draw_circle(xi,yi,sigma,color,.95f,1);
	}
}

//! Draw Gaussian spots from the given n x d list of coordinates and the size sigma
/**
   \param img image of dimension D where N spots are drawn
   \param coordinates coordinates of N spots as a CImg<> (N,D)
   \param sigma standard deviation of the Gaussian spots 
 **/
template<typename T1, typename T2>
void draw_spots_from_coordinates(CImg<T1> &img, const CImg<T2> &coordinates, const float sigma=1.f){
	if (!coordinates.is_empty()){
		if ( (img.depth() == 1) && (coordinates.height() >= 2)) {
			cimg_forX(coordinates, i){
				const T2 xi = coordinates(i, 0), yi = coordinates(i, 1);
				cimg_forXY_window(img, xi, yi, xj, yj, 3 * sigma, 3 * sigma) {
					img(xj,yj) += (T1)gaussian(xi, yi, xj, yj, sigma, sigma);
				}
			}
		} else if ( (img.depth() > 1) && (coordinates.height() >= 3) ) {
			cimg_forX(coordinates, i){
				const T2 xi = coordinates(i, 0), yi = coordinates(i, 1), zi = coordinates(i, 2);
				cimg_forXYZ_window(img, xi, yi, zi, xj, yj, zj, 3 * sigma, 3 * sigma, 3 * sigma) {
					img(xj,yj,zj) += (T1)gaussian(xi, yi, zi, xj, yj, zj, sigma, sigma, sigma);
				}

			}
		} else {
			throw CImgArgumentException("draw_spots_from_coordinates dimension missmatch: "
					"img.depth() is %d and coordinates space is %d.",
					img.depth(), coordinates.height());
		}
	}
}

//! Draw Gaussian spots from the given n x (d+1) list of coordinates + sigma + intensity
/**
   \param img image of dimension D where N spots are drawn
   \param coordinates coordinates and standard devition of N spots as a CImg<> (N,D+1)
 **/
template<typename T1, typename T2>
void draw_isotropic_gaussians(CImg<T1> &img, const CImg<T2> &coordinates){
	if (!coordinates.is_empty()){
		if ( (img.depth() == 1) && (coordinates.height() >= 2)) {
			cimg_forX(coordinates, i){
				const T2 xi = coordinates(i, 0), yi = coordinates(i, 1),
						sigmai = coordinates(i, 2), ai = coordinates(i, 3);
				const int p = cimg::max(1, 3 * sigmai), xc = cimg::round(xi), yc = cimg::round(yi);
				cimg_forXY_window(img, xc, yc, xj, yj, p, p) {
					img(xj,yj) += (T1) ai * (T1) gaussian(xi, yi, xj, yj, sigmai, sigmai);
				}
			}
		} else if ( (img.depth() > 1) && (coordinates.height() >= 3) ) {
			cimg_forX(coordinates, i){
				const T2 xi = coordinates(i, 0), yi = coordinates(i, 1), zi = coordinates(i, 2),
						sigmai = coordinates(i, 3), ai = coordinates(i, 4);
				const int p = cimg::max(1, 3 * sigmai), xc = cimg::round(xi), yc = cimg::round(yi), zc = cimg::round(zi);
				cimg_forXYZ_window(img, xc, yc, zc, xj, yj, zj, p, p, p) {
					img(xj,yj,zj) += (T1) ai * (T1) gaussian(xi, yi, zi, xj, yj, zj, sigmai, sigmai, sigmai);
				}

			}
		} else {throw CImgArgumentException("draw_spots_from_coordinates dimension missmatch: "
				"img.depth() is %d and coordinates space is %d.",
				img.depth(), coordinates.height());}
	}
}

//! return a threshold assuming the image values follow a Laplace distribution
/**
   \param img image
   \param pfa the error of 1st kind (probability of false alarm) of the Laplace distibution
 **/
template<typename T>
float laplace_threshold(const CImg<T> &img, float pfa=1e-4){
	const float
	pvalue = 1.0 - pfa,
	m = img.median(),
	s = (img - m).get_abs().mean();
	return m - s *(pvalue > 0.5 ? 1.0 : -1.0) * log(1.f - 2.f * fabs( pvalue - 0.5));
}

//! Estimate the parameters of the Laplace law
template<typename T>
void laplace_fit(const CImg<T> & img, float &m, float &s){
	m = img.median();
	s = (img - m).get_abs().mean();
}

//! Cumulative density function for Laplace distribution
template <typename T>
inline T laplace_cdf(const T x, const float m=0, const float s=1){
	const double z = (double)(x - m) / (double)s;
	return (T)((z < 0)?0.5*exp(z):1.0 - 0.5 * exp(-z));
}

//! Laplace cumulative distribution function
template <typename T>
CImg<T> laplace_cdf(const CImg<T> &img, const float m=0, const float s=1){
	CImg<T> dest(img);
	cimg_foroff(dest, i) laplace_cdf(dest(i),m,s);
	return dest;
}

//! keep the local maximas
template <typename T>
CImg<T> local_maxima(const CImg<T> &img, int size = 2){
	CImg<T> dest(img.width(),img.height(),img.depth(),img.spectrum(),0);
	cimg_forXYZ(img,xi,yi,zi){
		T maxi = cimg::type<T>::min();
		cimg_forXYZ_window(img,xi,yi,zi,xj,yj,zj,size,size,size) {
			if (img(xj,yj,zj) > maxi && !(xi == xj && yi ==yj && zj==zi)) {
				maxi = img(xj,yj,zj);
			}
		}
		if (img(xi,yi,zi) > maxi) { dest(xi,yi,zi) = 1; }
	}
	return dest;
}

//! keep the local maximas
template <typename T>
CImg<T> local_minima(const CImg<T> &img, int size = 2){
	CImg<T> dest(img.width(),img.height(),img.depth(),img.spectrum(),0);
	cimg_forXYZ(img,xi,yi,zi){
		T mini = cimg::type<T>::max();
		cimg_forXYZ_window(img,xi,yi,zi,xj,yj,zj,size,size,size) {
			if (img(xj,yj,zj) < mini && !(xi == xj && yi ==yj && zj==zi)) {
				mini = img(xj,yj,zj);
			}
		}
		if (img(xi,yi,zi) < mini) { dest(xi,yi,zi) = 1; }
	}
	return dest;
}

//! Remove isolated pixels in a binary image
template <typename T>
CImg<T> remove_islands(const CImg<T> &img){
	CImg<T> res(img);
	if (img.depth()==1){
		CImg_3x3(I,T);
		cimg_forC(img,c)  cimg_for3x3(img,x,y,0,c,I,T){
			if (Icc > 0){
				if (Ipc == 0 && Inc == 0 && Icp == 0 && Icn == 0){res(x,y,0,c) = 0;}
			}
		}
	} else {
		CImg_3x3x3(I,T);
		cimg_forC(img,c)  cimg_for3x3x3(img,x,y,z,c,I,T){
			if (Iccc > 0){
				if (Ipcc == 0 && Incc == 0 && Iccp == 0 && Iccn == 0 && Icpc == 0 && Icnc == 0){res(x,y,z,c) = 0;}
			}
		}
	}
	return res;
}

//! return the 2d coordinates of non zeros pixels
/**
   \return if the image is 2D return a Nx2 image, if the image is 3D
   return a Nx3 image.
 **/
template <typename T>
CImg<int> points_coordinates(const CImg<T> &img){
	CImg<int> coords;
	unsigned int N = 0;
	cimg_foroff(img,i) { if(img(i) > 0) { N++; }}
	if (img.depth() == 1){
		coords.assign(N,2,1,1,0);
		unsigned int i = 0;
		cimg_forXY(img,x,y) {
			if (img(x,y) > 0) {
				coords(i,0) = x;
				coords(i,1) = y;
				i++;
			}
		}
	} else {
		coords.assign(N,3,1,1,0);
		unsigned int i = 0;
		cimg_forXYZ(img,x,y,z) {
			if (img(x,y,z) > 0) {
				coords(i,0) = x;
				coords(i,1) = y;
				coords(i,2) = z;
				i++;
			}
		}
	}
	return coords;
}

//! return the 3d coordinates of centroid of the regions as a n x 2 image
template<typename T>
CImg<> centroids_of_regions(const CImg<unsigned int> &labels, const CImg<T> &img, const int min_size = 3){
	if (!labels)
		throw CImgArgumentException("get_centroids_of_regions : input labels is empty");
	if (!labels.is_sameXYZC(img))
		throw CImgArgumentException("get_centroids_of_regions : Specified label (%u,%u,%u,%u,%p) and "
				"image (%u,%u,%u,%u,%p) have different dimensions.",
				labels._width,labels._height,labels._depth,labels._spectrum,labels._data,
				img._width,img._height,img._depth,img._spectrum,img._data);

	CImg<> S(labels.max() + 1, 3, 1, 1, 0);
	CImgList<> coords;
	cimg_forXY(labels, x, y) {
		unsigned int i = labels(x,y);
		T w = img(x,y);
		S(i,0) += w;
		S(i,1) += w * x;
		S(i,2) += w * y;
	}

	cimg_forX(S, i)
	if (S(i,0) > min_size && i > 0) {
		const float x = S(i,1) /  S(i,0), y = S(i,2) /  S(i,0);
		coords.push_back(CImg<float>::vector(x,y));
	}

	return coords.get_append('x');
}


//! Mean shift estimation of the mode
/**
   \f$ m = \frac{\int x g(x,m)\,dx}{\int g(x,m)\,dx}   \f$
 **/
template<typename Ti, typename Tc>
CImg<> meanshift_localization(const CImg<Ti> &img, const CImg<Tc> &coordinates, const float sigma,
		const float epsilon=1e-3, const int max_iter=10000){
	CImg<> dest = coordinates;
	if (dest.is_empty()) return dest;
	if (img.depth() == 1){
		if (coordinates.height() != 2)
			throw CImgArgumentException("meanshift_localization: coordinates list and image dimension do not match");
#ifdef cimg_use_openmp
#pragma omp parallel for
#endif
		cimg_forX(coordinates, i){
			float xi = coordinates(i, 0), yi = coordinates(i, 1);
			float r = 1;
			for (int k = 0; (k < max_iter) && (r > epsilon); k++){
				float si = 0, xin = 0, yin = 0;
				cimg_forXY_window(img, xi, yi, xj, yj, 3 * sigma, 3 * sigma) {
					const float g = gaussian(xi, yi, xj, yj, sigma, sigma) * img(xj, yj);
					xin += g * xj;
					yin += g * yj;
					si  += g;
				}
				if (std::abs(si) > 1e-12){
					xin /= si; yin /= si;
					r = distance(xi,yi,xin,yin);
					xi = xin; yi = yin;
				} else {r = 0.f;}
			}
			dest(i, 0) = xi;
			dest(i, 1) = yi;
		}
	} else {
		if (coordinates.height() != 3)
			throw CImgArgumentException("meanshift_localization: coordinates list and image dimension do not match");
#ifdef cimg_use_openmp
#pragma omp parallel for
#endif
		cimg_forX(coordinates, i){
			float xi = coordinates(i, 0), yi = coordinates(i, 1), zi = coordinates(i, 2);
			float r = 1;
			for (int k = 0; (k < max_iter) && (r > epsilon); k++){
				float si = 0, xin = 0, yin = 0, zin = 0;
				cimg_forXYZ_window(img, xi, yi, zi, xj, yj, zj, 3 * sigma, 3 * sigma, 3 * sigma) {
					const float g = gaussian(xi, yi, zi, xj, yj, zj, sigma, sigma, sigma) * img(xj, yj, zj);
					xin += g * xj;
					yin += g * yj;
					zin += g * zj;
					si  += g;
				}
				if (std::abs(si) > 1e-12){
					xin /= si; yin /= si; zin /= si;
					r = distance(xi,yi,zi,xin,yin,zin);
					xi = xin; yi = yin; zi = zin;
				} else {r = 0.f;}
			}
			dest(i, 0) = xi;
			dest(i, 1) = yi;
			dest(i, 2) = zi;
		}
	}
	return dest;
}

//! Adjust the location parameter of a Gaussian model knowing its size (standard deviation)
/**
   \f$ m = \frac{\int x g(x,m)\left(a g(x,m) - u(x)\right)\,dx}{\int g(x,m)\left(a g(x,m) - u(x)\right)\,dx}   \f$
   and \f$ a = \frac{\int u(x) g(x,m)\,dx}{\int g(x,m)\,dx} \f$
   with
   \f$  g(x,m) = e^{-\frac{1}{2}\left(\frac{(x-m)}{\sigma}\right)^2} \f$
   \note Probably incorrect code
 **/
template<typename Ti, typename Tc>
CImg<> gaussian_blob_localization(const CImg<Ti> &img, const CImg<Tc> &coordinates, const float sigma,
		const float epsilon=1e-3, const int max_iter=10000){
	CImg<> dest = coordinates;
	if (dest.is_empty()) return dest;
	if (img.depth() == 1){
		if (coordinates.height() != 2)
			throw CImgArgumentException("gaussian_blob_localization: coordinates dimension (%d) and image depth (%s) do not match",
					coordinates.height(), img.depth()==1?"2D":"3D");
#ifdef cimg_use_openmp
#pragma omp parallel for
#endif
		cimg_forX(coordinates, i) {
			float xi = coordinates(i, 0), yi = coordinates(i, 1), ai = img(xi, yi);
			float r = 1;
			for (int k = 0; (k < max_iter) && (r > epsilon); k++){
				float sg = 0, sggu =0, sggux = 0, sgguy = 0, sgu = 0;
				int xc = cimg::round(xi), yc = cimg::round(yi);
				cimg_forXY_window(img, xc, yc, xj, yj, 3 * sigma, 3 * sigma) {
					const float g = gaussian(xi, yi, xj, yj, sigma, sigma), u = img(xj, yj), ggu = g * (ai * g - u);
					sggux += ggu * xj;
					sgguy += ggu * yj;
					sggu  += ggu;
					sgu   += g * u;
					sg    += g;
				}
				if (fabs(sg) > 1e-12 && fabs(sggu) > 1e-12){
					sggux /= sggu; sgguy /= sggu;
					r = distance(xi,yi,sggux,sgguy);
					xi = sggux; yi = sgguy; ai = sgu / sg;
				} else {r = 0.f;}
			}
			dest(i, 0) = xi;
			dest(i, 1) = yi;
		}
	} else {
		if (coordinates.height() != 3)
			throw CImgArgumentException("gaussian_blob_localization: coordinates dimension (%d) and image depth (%s) do not match",
					coordinates.height(), img.depth()==1?"2D":"3D");
#ifdef cimg_use_openmp
#pragma omp parallel for
#endif
		cimg_forX(coordinates, i){
			float xi = coordinates(i, 0), yi = coordinates(i, 1), zi = coordinates(i, 2), ai = img(xi,yi,zi);
			float r = 1;
			for (int k = 0; (k < max_iter) && (r > epsilon); k++){
				float sg = 0, sggu =0, sggux = 0, sgguy = 0, sgguz = 0, sgu = 0;
				cimg_forXYZ_window(img, (int)xi, (int)yi, zi, xj, yj, zj, 3 * sigma, 3 * sigma, 3 * sigma) {
					const float g = gaussian(xi, yi, zi, xj, yj, zj, sigma, sigma, sigma),
							u = img(xj, yj, zj), ggu = g * (ai * g - u);
					sggux += ggu * xj;
					sgguy += ggu * yj;
					sgguz += ggu * zj;
					sggu  += ggu;
					sgu   += g * u;
					sg    += g;
				}
				if (fabs(sg) > 1e-12 && fabs(sggu) > 1e-12){
					sggux /= sggu; sgguy /= sggu; sgguz /= sggu;
					r = distance(xi,yi,zi,sggux,sgguy,sgguz);
					xi = sggux; yi = sgguy; zi = sgguz; ai = sgu / sg;
				} else {r = 0.f;}
			}
			dest(i, 0) = xi;
			dest(i, 1) = yi;
			dest(i, 2) = zi;
		}
	}
	return dest;
}

//! Blob / spot response filter
/**
 \param img The input image
 \param feature Tree type of feature are implemented: Gaussian Curvature, DoG and LoG, Harris
 \note The Gaussian curvature returns the determinent of the Hessian
       matrix and the Harris return the minimum value of the structure
       tensor.
 **/
template <typename T>
CImg<> blob(const CImg<T> &img, int feature=0, float scale=2){
	CImg<> dest;
	switch(feature){
	case 0: { // gaussian curvature
		CImg<> H = img.get_blur(2*scale).get_hessian().get_append('c');
		dest.assign(img.width(),img.height(),img.depth(),img.spectrum());
		cimg_forXYZ(H,x,y,z) dest(x,y,z) = H.get_tensor_at(x,y,z).det();
		break;
	}
	case 1: { // Difference of Gaussians
		dest = img.get_blur(scale) - img.get_blur(2 * scale); break;
	}
	case 2: { // LoG
		dest = -img.get_blur(scale).laplacian(); break;
	}
	case 3: { // Harris
		dest = img.get_blur(scale).structure_tensors().blur(scale);
#pragma omp parallel for
		cimg_forXYZ(dest, x, y, z) {
			CImg<> val, vec;
			dest.get_tensor_at(x,y,z).symmetric_eigen(val,vec);
			dest(x,y,z) = val.min();
		}
		dest.channel(0);
	} break;
	default: {
		return img.get_blur(scale);
	}
	}
	return dest;
}


//! sigmoid ((1+tanh)/2) function
template <typename T>
T sigmoid(T x){
	return 0.5 * (1.0 + tanh(x));
}

//! inverse sigmoid  1/2 log(p / (1.0 - p)) function
template <typename T>
T sigmoid_inv(T p){
	return 0.5 * log(p / (1.0 - p));
}

//! detect blobs and return their coordinates
/**
   \param img The input image
   \param feature Tree type of feature are implemented: 0:Gaussian Curvature, 1:DoG and 2:LoG, 3:Harris
   \param scale the scale of the blob
   \param pfa a probability of false alarm (error of 1st kind) for detection of the blob
          (a laplacian distribution of the score is assumed for feature )
   \return the coordinates of the blobs with pixel accuracy
   \note the blob are local maximas with intensities above the threshold given by the probability
         of false alarm. For the Harris an heuristic approach is taken for thresholding.
 **/
template <typename T>
CImg<int> detect_blob(const CImg<T> &img, int feature=1, float scale=2, float pfa=1e-4){
	CImg<> score = blob(img, feature, scale);
	if (feature==3) {
		const float threshold = sigmoid_inv(pfa) * std::sqrt(score.variance());
		score = local_maxima(score, 1).mul(remove_islands(score.get_threshold(threshold)));
	} else {
		score = local_maxima(score, 1).mul(remove_islands(score.get_threshold(laplace_threshold(score, pfa))));
	}
	return points_coordinates(score);
	//coords = centroids_of_regions(score.get_threshold(laplace_threshold(score,p)).get_label(), img);
}

//! convert the coordinates list to homogene coordinates by adding a 1 as first coordinates
/**
   \param a list of coordinates as an images of width N and height 2.
   \return a list of coordinates as an images of width N and height 3.
 **/
template <typename T>
CImg<T> to_homogene_coords(const CImg<T> &coords){
	CImg<T> dest(coords.width(), coords.height() + 1);
	cimg_forX(dest,x) dest(x,0) = (T)1;
	cimg_forXY(coords,x,y) dest(x, y + 1) = coords(x, y);
	return dest;
}

//! compute the distance matrix between two set of points defined by their coordinates
/**
 \param points0 a list of d-dimensional coordinates as an images of width N and height d.
 \param points1 a list of d-dimensional coordinates as an images of width M and height D.
 \return an image of width N and height M.
 **/
template <typename T>
CImg<> distance_matrix(const CImg<T> &points0, const CImg<T> & points1){
	CImg<> dmatrix(points0.width(), points1.width(), 1, 1);
	cimg_forX(points0, i) {
		CImg<> Pi = points0.get_crop(i,0,i,points0.height()-1);
		cimg_forX(points1, j) {
			CImg<> Pj = points1.get_crop(j,0,j,points1.height()-1);
			dmatrix(i, j) = sqrt((Pi-Pj).sqr().sum());
		}
	}
	return  dmatrix;
}

//! return a simple rough assignment as a permutation p(col)=row
/**
    \param a cost matrix as an image of width N and height M.
    \note if no assignment then return p(col)=-1
 **/
template <typename T>
CImg<int> assignment(CImg<T> cost){
	// substract the minimum of each line to each line
	cimg_forY(cost, y) cost.get_shared_row(y) -= cost.get_shared_row(y).min();
	// substract the minimum of each column to each column
	cimg_forX(cost, x){
		const T mini = cost.get_column(x).min();
		cimg_forY(cost, y) cost(x, y) -= mini;
	}
	CImg<int> perms(cost.width(),1,1,1,-1);
	// on assigment is done if there is only one zero per row
	CImg<bool> deleted(cost.width(), cost.height(),1,1,false);
	cimg_forXY(cost, x, y){
		if (cost(x,y) == 0 && !deleted(x,y) ) {
			perms(x) = y;
			cimg_forX(deleted, xx) deleted(xx,y) = true;
			cimg_forY(deleted, yy) deleted(x,yy) = true;
		}
	}
	return perms;
}

//! Extract rows from image src according to row indexed in permutations
/**
   \note can be used to permute the rows of the image.
 **/
template<typename T>
CImg<T> extract_rows(const CImg<T> &src, const CImg<int> &permutations){
	int n=0;
	cimg_foroff(permutations, i) if (permutations(i) > 0) n++;
	CImg<> dest(n, src.height());
	n = 0;
	cimg_foroff(permutations, i){
		if (permutations(i) > 0) {
			cimg_forY(dest, j) { dest(n,j) = src(permutations(i));}
			n++;
		}
	}
	return dest;
}

//! reorder coordinates1 in order match coordinates 0
template<typename T>
CImg<T> min_distance_assignment(const CImg<T> & coordinates0, const CImg<T> & coordinates1){
	return extract_rows(coordinates1, assignment(distance_matrix(coordinates0, coordinates1)));
}

template<typename T>
CImgList<T> assignment(const CImgList<T> coords){
	CImgList<T> ncoords0, ncoords1;
	CImg<int> perms = assignment(distance_matrix(coords[0], coords[1]));
	cimg_forX(perms,i)
	if (perms(i) > 0){
		ncoords0.push_back(coords[0].get_column(i));
		ncoords1.push_back(coords[1].get_column(perms(i)));
	}
	CImgList<> dest(2);
	dest[0] = ncoords0.get_append('x');
	dest[1] = ncoords1.get_append('x');
	return dest;
}


//! find the nearest neighbor bewteen two set of points defined by a list of coordinates
template<typename T>
CImgList<T> nearest_neighbor(const CImgList<T> &coords, float dmax){
	CImgList<> ncoords0, ncoords1;
	// matching
	cimg_forX(coords[0], i) {
		int xi = coords[0](i,0), yi = coords[0](i,1);
		float d0 = 1e6;
		int j0 = -1;
		cimg_forX(coords[1], j){
			int xj = coords[1](j,0), yj = coords[1](j,1);
			float dx = xj - xi, dy = yj - yi, d = dx * dx + dy * dy;
			if (d < d0) {d0 = d; j0 = j;}
		}
		if (j0 > 0 && d0 < dmax * dmax) {
			ncoords0.push_back(coords[0].get_column(i));
			ncoords1.push_back(coords[1].get_column(j0));
		}
	}
	CImgList<> dest(2);
	dest[0] = ncoords0.get_append('x');
	dest[1] = ncoords1.get_append('x');
	return dest;
}

/**
   \section Affine landmark based registration
   \note useful for registration of dualview images from a calibration image (beads)
 **/

//! visualize the matching of points
inline void visu_registration(const CImg<> &img, const CImgList<> &coords){
	CImg<> visu = img;
	cimg_forC(visu,c)
	visu.get_shared_channel(c).normalize(0,255);
	float col0[2]={255,0}, col1[2]={0,255}, col2[2]={255,255};
	cimg_forX(coords[0],i){
		int xi = coords[0](i,0), yi = coords[0](i,1);
		int xj = coords[1](i,0), yj = coords[1](i,1);
		visu.draw_circle(xi,yi,1,col0);
		visu.draw_circle(xj,yj,1,col1);
		visu.draw_line(xi,yi,xj,yj,col2);
	}
	visu.display("registration",false);
}

//! normalize point cloud to better conditionate the estimation of the homography
inline CImg<> normalize2dpts(const CImg<> &coords){
	if (coords.is_empty()) return CImg<>::identity_matrix(3);
	float
	cx = coords.get_shared_row(1).mean(),
	cy = coords.get_shared_row(2).mean(),
	sx = 1./sqrt(coords.get_shared_row(1).variance()),
	sy = 1./sqrt(coords.get_shared_row(2).variance());
	CImg<> T(3,3);
	T.fill(1  ,   0,  0,
			-cx*sx,   sx,  0,
			-cy*sy,   0,  sy);
	return T;
}

//! Estimate the affine transform from two set of points coordinates
template<typename Tp>
CImg<> estimate_affine_transform(const CImgList<Tp> & points,
		bool normalize=true,
		bool robust=true){
	if (points.size() < 2)
		throw CImgException("estimate_affine_transform : not enough set of coordinates");
	if (points[0].width() != points[1].width())
		throw CImgException("estimate_affine_transform : coordinate list have not the same size");
	CImgList<> coords(2);
	coords[0] = to_homogene_coords(points[0]);
	coords[1] = to_homogene_coords(points[1]);
	CImgList<> T(2); CImg<> X,Y,M;
	if (normalize){
		T[0] = normalize2dpts(coords[0]);
		T[1] = normalize2dpts(coords[1]);
		X = (T[0] * coords[0]).get_transpose();
		Y = (T[1] * coords[1]).get_transpose();
	} else {
		X = coords[0].get_transpose();
		Y = coords[1].get_transpose();
	}
	M = X.get_pseudoinvert() * Y;
	if (robust){
		CImg<> delta = M * X.get_transpose() - Y;
		float
		sx = 2 * sqrt(delta.get_row(1).variance()),
		sy = 2 * sqrt(delta.get_row(2).variance());
		for (int k = 0; k < 10; k++){
			CImg<> Xt(X);
			cimg_forXY(X, i, j){
				float dx = delta(i,1)/sx, dy = delta(i,2)/sy, w = exp(-0.5f*(dx*dx+dy*dy));
				Xt(i,j) = X(i,j) * w;
			}
			Xt.transpose();
			M = (Xt*X).invert() * Xt * Y;
			delta = M * X.get_transpose() - Y;
		}
	}
	if (normalize) M = T[1].invert() * M.transpose() * T[0];
	else M.transpose();
	return M;
}

//! Apply an 2D affine transform M to an image img
/*
  Take into account a possible additional translation of the original data
  (due to a crop for example)
 */
template<typename Ti>
CImg<Ti> apply_affine_transform(const CImg<Ti> &img,  const CImg<> & M,
		const float x0 = 0, const float y0 = 0){
	if (!M.is_empty()) {
		CImg<> dest(img.width(), img.height(), img.depth(), img.spectrum(), 0);
		CImg<> X(1, 3), Y(1, 3), T1(3, 3),T2(3, 3),T;
		T1.fill(1,  0, 0,
				x0, 1, 0,
				y0, 0, 1);
		T2.fill(1,  0, 0,
				-x0, 1, 0,
				-y0, 0, 1);
		T = T2 * M * T1;
		cimg_forXY(dest, x, y) {
			X.fill(1, x, y);
			Y = T * X;
			cimg_forZC(dest, z, c)
			dest(x, y, z, c) = img.cubic_atXY(Y(1), Y(2), z, c);
		}
		return dest;
	} else {
		return img;
	}
}

/**
   \section Complex manipulation
 **/

template<typename T>
CImgList<T> complex_conjugate(const CImgList<T> &z){
	CImgList<> dest(2);
	dest[0] = z[0];
	dest[1] = -z[1];
	return dest;
}

template<typename T>
CImgList<T> complex_add(const CImgList<T> &z1, const CImgList<T> &z2){
	if (!z1.is_sameXYZC(z2)) throw CImgArgumentException("complex_add inputs have not the same size.");
	CImgList<> dest(z1);
	cimg_foroff(dest[0],i){
		const T a = z1[0](i), b = z1[1](i), c = z2[0](i), d = z2[1](i);
		dest[0](i) = a + c;
		dest[1](i) = b + d;
	}
	return dest;
}

template<typename T>
CImgList<T> complex_diff(const CImgList<T> &z1, const CImgList<T> &z2){
	if (!z1.is_sameXYZC(z2)) throw CImgArgumentException("complex_diff inputs have not the same size.");
	CImgList<> dest(z1);
	cimg_foroff(dest[0],i){
		const T a = z1[0](i), b = z1[1](i), c = z2[0](i), d = z2[1](i);
		dest[0](i) = a - c;
		dest[1](i) = b - d;
	}
	return dest;
}

template<typename T>
CImgList<T> complex_mul(const CImgList<T> &z1, const CImgList<T> &z2){
	if (!z1.is_sameXYZC(z2)) throw CImgArgumentException("complex_mul inputs have not the same size.");
	CImgList<> dest(z1);
	cimg_foroff(dest[0],i){
		const T a = z1[0](i), b = z1[1](i), c = z2[0](i), d = z2[1](i);
		dest[0](i) = a * c - b * d;
		dest[1](i) = b * c + a * d;
	}
	return dest;
}

template<typename T>
CImgList<T> complex_scalar_mul(const T lambda, const CImgList<T> &z){
	CImgList<> dest(z);
	cimg_foroff(dest[0],i){
		dest[0](i) *= lambda;
		dest[1](i) *= lambda;
	}
	return dest;
}

template<typename T>
CImgList<T> complex_div(const CImgList<T> &z1, const CImgList<T> &z2){
	if (!z1.is_sameXYZC(z2)) throw CImgArgumentException("complex_div inputs have not the same size.");
	CImgList<> dest(z1);
	cimg_foroff(dest[0],i){
		const T a = z1[0](i), b = z1[1](i), c = z2[0](i), d = z2[1](i), cd=c*c+d*d;
		dest[0](i) = (a * c + b * d) / cd;
		dest[1](i) = (b * c - a * d) / cd;
	}
	return dest;
}

template<typename T>
CImg<T> complex_abs(const CImgList<T> &z){
	return (z[0].get_sqr()+z[1].get_sqr()).sqrt();
}

//! Compute the convolution between two images using the Fast Fourier Transform
/**
  \param img0 : first image
  \param img1 : second image
  \note Computed as \f$ \mathrm{Re}(\mathcal{F}^{-1}(\mathcal{F}(f)\mathcal{F}(g))) \f$
 **/
template<typename T>
CImg<> convolve_fft(const CImg<T> &f, const CImg<T>&g){
	if (!f.is_sameXYZC(g)) throw CImgArgumentException("convolve_fft inputs have not the same size.");
	return (complex_mul(f.get_FFT(), g.get_FFT()).FFT(true))[0].shift(f.width()/2,f.height()/2,f.depth()/2,0,2);
}

//! Compute the correlation between two images using the Fast Fourier Transform
/**
  \param f : first image
  \param g : second image
  \note Computed as \f$  \mathrm{Re}(\mathcal{F}^{-1}(\mathcal{F}(f)\mathcal{F}(g)^{*})) \f$
 **/
template<typename T>
CImg<> correlate_fft(const CImg<T> & f, const CImg<T> &g){
	if (!f.is_sameXYZC(g)) throw CImgArgumentException("correlate_fft inputs have not the same size.");
	return ((complex_mul(f.get_FFT(), complex_conjugate(g.get_FFT())).FFT(true))[0]).shift(f.width()/2,f.height()/2,f.depth()/2,0,2);
}

//! Compute the autocorrelation of the image using the Fast Fourier Transform
/**
  \param img : input image
  \note Computed as \f$ \mathrm{Re}(\mathcal{F}^{-1}(\mathcal{F}(f)\mathcal{F}(f)^{*})) \f$
 **/
template<typename T>
CImg<T> autocorrelate_fft(const CImg<T> &img){
	return correlate_fft(img, img);
}

//! Threshold the Fourier coefficient
template<typename T>
CImgList<T> threshold_fft(const CImgList<T> &z, const float threshold){
	CImgList<> dest(z);
	if (threshold > 0){
		cimg_foroff(z[0],i) {
			const T mod = sqrt(z[0](i)*z[0](i) + z[1](i)*z[1](i));
			if ( mod < threshold){
				dest[0](i) = 0;
				dest[1](i) = 0;
			} else {
				const T phi = atan2(z[1](i),z[0](i)), rho = mod - threshold;
				dest[0](i) = rho * cos(phi);
				dest[1](i) = rho * sin(phi);
			}
		}
	}
	return dest;
}

//! Symmetrize an image
/**
   \param img the input image
   \note The reverse operation resize(img,0) : img == symetrize(img).resize(img,0)
 **/
template<typename T>
CImg<T> symetrize(const CImg<T> &img,const bool invert=false){
	CImg<T> dest(img);
	if (invert){
		dest.resize(img.width()/2,img.height()/2,cimg::max(img.depth()/2,1),img.spectrum(),0);
	} else {
		if (img.width()>1)  dest.append(dest.get_mirror('x'),'x');
		if (img.height()>1) dest.append(dest.get_mirror('y'),'y');
		if (img.depth()>1)  dest.append(dest.get_mirror('z'),'z');
	}
	return dest;
}

//! Whitening filter
/**
   \note \f$ W(f) = \frac{S(f)}{|S(f)|} \f$
 **/
template<typename T>
CImg<T> whiten_noise(const CImg<T> &img, const float alpha=.25, bool do_symetrize = true){
	const T mu = img.mean();
	CImgList<T> ft;
	if (do_symetrize) ft = (symetrize(img - mu)).get_FFT();
	else ft = (img - mu).get_FFT();
	CImg<T> psd = complex_abs(ft);
	psd.pow(alpha);
	psd.max(1e-12);
	ft[0].div(psd);
	ft[1].div(psd);
	ft.FFT(true);
	if (do_symetrize) return ft[0].resize(img,0) + mu;
	else return ft[0] + mu;
}


//! Van-Cittert deconvolution
/**
   \param img input image
   \param psf point spread function
   \param alpha step size
   \param max_iter the maximum number of iterations
   \param positivity apply the positivity constraint
   \param smoothness apply the smoothness constraint
   The iteration is
   \f$  f_{n+1} = f_{n} - \alpha (h * f_{n} - g ) \f$
   Uses positivity constraint and smoothing according to the option.   
 **/
template<typename T>
CImg<T> deconvolve_vancittert(const CImg<T> &img, const CImg<T> &psf, const int max_iter=10, 
		const T alpha = 1, const bool positivity=false, const float smoothness=0){
	CImg<T> h(psf), u(img);
	if (!h.is_sameXYZC(img)) h.resize(img,0,0,0.5,0.5,0.5,0);
	h /= h.sum();
	CImgList<T> fft_h = h.get_FFT(), fft_u;
	for (int k = 0; k <= max_iter; k++){
		fft_u = u.get_FFT();
		u += alpha * (img - convolve_fft(h, u));
		if (positivity) u.max(0.f);
		if (smoothness > 0) u.blur(smoothness);
	}
	return u;
}

//! Averages patches centered around a list of given coordinates
/**
   \param img input source image
   \param coords a Nx3 coordinates table
   \param width  width of the destination image
   \param height  height of the destination image
   \param depth depth of the destination image
 **/
template<typename T, typename Tc>
CImg<> average_patches(const CImg<T> &img, const CImg<Tc> &coords, int width, int height, int depth){
	CImg<> dest(width,height,depth,img.spectrum(),0), normalization(width,height,depth,img.spectrum(),0);
#ifdef cimg_use_openmp
#pragma omp parallel for
#endif
	cimg_forXYZ(dest,x,y,z){
		cimg_forX(coords, i){
			const float
			X = (float)(x - 0.5 * width)  + coords(i,0),
			Y = (float)(y - 0.5 * height) + coords(i,1),
			Z = (float)(z - 0.5 * depth)  + coords(i,2);
			if (img.containsXYZC((int)X,(int)Y,(int)Z,0)){
				dest(x,y,z) += img.cubic_atXYZ(X,Y,Z);
				normalization(x,y,z) += 1.f;
			}
		}
	}
	cimg_foroff(dest,i) if (normalization(i)>0) dest(i) /= normalization(i);
	return dest;
}

//! Normalize two images togethers assuming a linear relationship between the intensities
template<typename T>
CImg<T> normalize(const CImg<T> &ref, const CImg<T> &img){
	CImg<> x = ref.get_unroll('y'), y = img.get_unroll('y'), A(x.size(), 2);
	A.get_shared_row(0) = 1.f;
	A.get_shared_row(1) = x;
	CImg<> fit = A.transpose().pseudoinvert() * y;
	return CImg<T>((img - fit(0)) / fit(1));
}

//! Normalize the channels among each other
template <typename T>
CImg<T> normalize_channels(const CImg<T>&img){
	CImg<T> dest(img);
	for (int c = 1; c < img.spectrum(); c++)
		dest.get_shared_channel(c) = normalize( img.get_shared_channel(0), img.get_shared_channel(c));
	return dest;
}

//! Phase correlation for image alignments
/**
   \param f reference image
   \param g translated image
   \param dx horizontal displacment (returned value)
   \param dy vertical displacment (returned value)
   \note the phase correlation is computed as
   \f$ \arg\max \mathcal{F}^{-1}\left\{\frac{\mathcal{F}(f) \mathcal{F}(f)*}{|\mathcal{F}(f) \mathcal{F}(f)*|}\right\}  \f$
   Both images should have the same size
 **/
template<typename T>
void phase_correlation(const CImg<T> &f, const CImg<T>& g, int & dx, int & dy) {
	if(!f.is_sameXYZ(g))
		throw CImgArgumentException("phase correlation(const CImg<T>&, const CImg<T>&, int &, int &) : "
				"Instance image (%u,%u,%u,%u) and "
				"reference image (%u,%u,%u,%u) "
				"must have same dimensions",
				g.width(), g.height(), g.depth(), g.spectrum(),
				f.width(), f.height(), f.depth(), f.spectrum());
	// Compute the phase correlation
	CImgList<> prod = complex_mul(f.get_FFT(), complex_conjugate(g.get_FFT()));
	CImg<> prodmod = complex_abs(prod);
	prod[0].div(prodmod);
	prod[1].div(prodmod);
	prod.FFT(true);
	CImg<> norm = complex_abs(prod);
	// Find the maximum
	float maxi = norm(0);
	dx = 0;
	dy = 0;
	cimg_forXY(norm, x, y)
	if (norm(x,y) > maxi) {
		maxi = norm(x,y);
		dx = x;
		dy = y;
	}
	if (dx > f.width()/2 - 1)  dx = f.width() - dx;
	if (dy > f.height()/2 - 1) dy = f.height() - dy;
}

//! \section Patch-based event detection using Generalized Extreme Distribution

//! GEV cumulative distribution [Alliot ESAM08]
/**
   \f[
   F(x) = \left\{ \begin{array}{ll}
   \exp(-(1-\kappa\frac{x-\beta}{\alpha})^{1/k}) & if \kappa \neq 0 \\
   \exp(-\exp(-\frac{x-\beta}{\alpha}))  & if \kappa \neq 0 \\
   \end{array}
   \right.
   \f]
 */
inline double gevcdf(const double x, const double alpha,
		const double beta, const double kappa){
	if (kappa!=0) {
		const double a = 1.0-kappa*(x-beta)/alpha;
		return a>0?exp(-pow(a,1.0/kappa)) : 0;
	}
	else return exp(-exp(-(x-beta)/alpha));
}

//! GEV cumulative distribution [Alliot ESAM08]
template<typename T>
CImg<double> gevcdf(const CImg<T> & x, const double alpha,
		const double beta, const double kappa){
	CImg<> dest(x,false);
	const   int n=x.size();
#ifdef cimg_use_openmp
#pragma omp parallel for
#endif
	for ( int i=0;i<n;++i)
		dest(i)=gevcdf(x(i),alpha,beta,kappa);
	return dest;
}

//! GEV pdf
inline double gevpdf(const double x, const double alpha, const double beta, const double kappa){
	if (kappa!=0) {
		const double a = 1.0-kappa*(x-beta)/alpha;
		return a>0?pow(a,1.0/kappa-1.0)*exp(-pow(a,1.0/kappa)) / alpha : 0;
	}
	else return -exp(-(x-beta)/alpha)*exp(-exp(-(x-beta)/alpha))/alpha;
}

//! GEV pdf
template<typename T>
CImg<double> gevpdf(const CImg<T> & x, const double alpha, const double beta, const double kappa){
	CImg<> dest(x,false);
	const int  n=x.size();
#ifdef cimg_use_openmp
#pragma omp parallel for
#endif
	for (int i=0;i<n;++i)
		dest(i)=gevpdf((double)x(i),alpha,beta,kappa);
	return dest;
}

//! GEV log-likelihood
template<typename T>
inline double gevlnlh(const CImg<T> & x, const double alpha, const double beta, const double kappa){
	if (alpha<0) return cimg::type<double>::min();
	double A=0.0,B=0.0,n=0.0;
	cimg_for(x,ptr,T) {
		const double delta=1.0-kappa*(*ptr-beta)/alpha;
		if (delta>=0.0) {
			A += pow(delta,1.0/kappa);
			B += log(delta);
			n++;
		}
	}
	if (n==0) return cimg::type<double>::min();
	return -n*log(alpha)-A+(1.0/kappa-1.0)*B;
}

//! p-quantile of the GEV distribution
inline double gevquantile(const double p, const double alpha, const double beta, const double kappa){
	if (p<0) throw CImgException("gevquantile: p (=%.2f) is negative",p);
	if (p>1) throw CImgException("gevquantile: p (=%.2f) is more than 1",p);
	if (kappa!=0) return alpha/kappa*(1.0-pow(-log(p),kappa))+beta;
	else return beta-alpha*log(-log(p));
}

//! GEV parameters estimation
/**
   - [Alliot ESAM08] mix moment ml method
   - [Morrison J.E. and Smith J.A. (2002) Stochastic modeling of flood peaks using the generalized extreme value distribution. Water Resources Research 38, 1305]
 */
template<typename T>
void gevfit(const CImg<T> & x, double & alpha,
		double & beta,  double & kappa, const int sign_kappa=0) {
	// L-Moments
	const int n=x.size();
	const double lambda1=x.mean();
	double lambda2=0;
#ifdef cimg_use_openmp
#pragma omp parallel for schedule(dynamic,500) reduction(+:lambda2)
#endif
	for (int j=0;j<n;++j) {
		const double xj=x(j);
		double dl=0;
		const T * ptr_i=x.data();
		for (int i=0;i<j;i++) dl+= cimg::abs((*ptr_i++)-xj);
		lambda2 += dl/n;
	}
	lambda2/=(n-1);
	// Tries to guess the sign of kappa from the difference median-mean
	double l0,l1,e=.7;
	if (sign_kappa==0){
		const double med=x.median();
		l0=(lambda1-med)/lambda2>.21?-e:.01; l1=(lambda1-med)/lambda2>.21?-0.01:e;
	}else{
		l0=sign_kappa<0?-e:0.0;  l1=sign_kappa<0?0.0:e;
	}
	// Maximum-Likelihood line search
	const int sz=25;
	CImg<double> buf(sz);
#ifdef cimg_use_openmp
#pragma omp parallel for schedule(dynamic,sz/8)
#endif
	for (int i=0;i<sz;++i){
		const double
		lambda3 = l0+(l1-l0)/(sz-1)*i,
		a = tgamma(1.0+lambda3),
		b = (1.0-pow(2.0,-lambda3))*a,
		c = (lambda2*lambda3) / b,
		d = lambda1 - lambda2*(1-a) / b,
		e = gevlnlh(x,c,d,lambda3);
		buf(i,0)=e;
	}
	double mlmax=cimg::type<double>::min();
	for (int i=0;i<sz;++i){const double e=buf(i);
	if (e>mlmax) { mlmax=e; kappa= l0+(l1-l0)/(sz-1)*i;}
	}
	const double a = tgamma(1.0+kappa), b = (1.0-pow(2.0,-kappa))*a;
	alpha = (lambda2*kappa) / b;
	beta = lambda1 - lambda2*(1.0-a) / b;
#if cimg_debug>=3
	printf("gevfit : n = %d, lambda1 = %f, lambda2 = %f, alpha=%f, beta = %f, kappa = %f, mlmax = %f\n",n,lambda1,lambda2, alpha, beta, kappa, mlmax);
#endif
}

//! Compute the minimum distance between 2D blocks in a 2D+t neighborhood for each points of the input image
template<typename T>
CImg<> min_block_distance(const CImg<T>  &img, const int p, const int w){
	CImg<> dest(img.width(),img.height(),img.depth(),1,0);
#pragma omp parallel for
	cimg_forXYZ(img,xi,yi,zi){
		float d1 = cimg::type<float>::max();
		cimg_forXYZ_window(img,xi,yi,zi,xj,yj,zj,w,w,1) {
			if (zj != zi) {
				float d = 0;
				cimg_forXYZ_patch(img,xi,yi,zi,xj,yj,zj,xi_,yi_,zi_,xj_,yj_,zj_,p,p,0){
					const float val = (float)img(xi_, yi_, zi_) - (float)img(xj_, yj_, zj_);
					d += val * val;
				}
				if (d < d1) d1 = d;
			}
		}
		dest(xi,yi,zi) = d1;
	}
	return dest;
}

//! Patch based event detection
template<typename T>
CImg<> pbed(const CImg<T> &img, const int patch_size, const int window_size,
		const float pvalue = 1e-2f){
	CImg<> score = min_block_distance(img, patch_size, window_size);
	double alpha = 0, beta = 0, kappa = 0;
	gevfit(score, alpha, beta, kappa);
	const float t = gevquantile(1.f - pvalue, alpha, beta, kappa);
	return score.threshold(t);
}

//! compute a sum along a dimension of the image
/**
   \param img an input image
   \param axis can be 'x','y','z','c'
 **/
template<typename T>
CImg<T> sum(const CImg<T> & img, const char axis='y'){
	CImg<T> dest;
	if (axis == 'x') {
		dest.assign(1,img._height,img._depth,img._spectrum,0);
		cimg_forXYZC(img,x,y,z,c){ dest(0,y,z,c) += img(x,y,z,c);}
	} else if (axis == 'y'){
		dest.assign(img._width,1,img._depth,img._spectrum,0);
		cimg_forXYZC(img,x,y,z,c){ dest(x,0,z,c) += img(x,y,z,c);}
	} else if (axis == 'z'){
		dest.assign(img._width,img._height,1,img._spectrum,0);
		cimg_forXYZC(img,x,y,z,c){ dest(x,y,0,c) += img(x,y,z,c);}
	} else if (axis == 'c'){
		dest.assign(img._width,img._height,img._depth,1,0);
		cimg_forXYZC(img,x,y,z,c){ dest(x,y,0,c) += img(x,y,z,c);}
	} else
		throw CImgArgumentException("sum(const CImg<>, const char): invalid axis name.");
	return dest;
}


//! compute the mean along a dimension of the image
/**
   \param img an input image
   \param axis can be 'x','y','z','c'
 **/
template<typename T>
CImg<T> mean(const CImg<T> & img, const char axis='y'){
	if (axis == 'x') return sum(img, axis) / img.width();
	else if (axis == 'y') return sum(img, axis) / img.height();
	else if (axis == 'z') return sum(img, axis) / img.depth();
	else if (axis == 'c') return sum(img, axis) / img.spectrum();
	else
		throw CImgArgumentException("mean(const CImg<>, const char): invalid axis name.");
}

//! Compute the covariance matrix of x as a collection of vectors
/**
   \param img a 2D image
   \return ((img-m)*(img-m).get_transpose())/(w.width()-1)
 **/
template<typename T>
CImg<> covariance(const CImg<T> &img){
	if (img.depth()!=1 && img.spectrum()!=1)
		throw CImgArgumentException("Covariance(const CImg<T> &): argument is not a 2D image (%dx%dx%dx%d).",
				img.width(),img.height(),img.depth(),img.spectrum());
	CImg<> dest = img, m = mean(img,'x');
	cimg_forXY(dest,x,y) dest(x,y) -= m(y);
	dest = dest * dest.get_transpose();
	return dest/(dest.width()-1);
}


//! local variance
template<typename T>
CImg<T> blur_local_variance(const CImg<T> & img, int n, int variance_method=1){
	CImg<T> res(img.width(),img.height(),img.depth(),img.spectrum());
	T *ptrd = res._data;
	const int hl = n/2, hr = hl - 1 + n%2;
	cimg_forXYZC(res,x,y,z,c) { // 3d
		const int
		x0 = x - hl, y0 = y - hl, z0 = z-hl, x1 = x + hr, y1 = y + hr, z1 = z+hr,
		nx0 = x0<0?0:x0, ny0 = y0<0?0:y0, nz0 = z0<0?0:z0,
				nx1 = x1>=img.width()?img.width()-1:x1, ny1 = y1>=img.height()?img.height()-1:y1,
						nz1 = z1>=img.depth()?img.depth()-1:z1;
		*(ptrd++) = img.get_crop(nx0,ny0,nz0,c,nx1,ny1,nz1,c).variance(variance_method);
	}
	return res;
}

//! estimate the parameters of the CCD noise
/**
   \return a vector containing [edc,g0]
   The noise model is \f$ Var[Y] = g_0 E[Y] + e_{dc} \f$
 **/
template<typename T>
CImg<T> estimate_ccd_noise_parameters(const CImg<T> &img, const float scale=1, const bool robust=true, const bool visu=false){
	CImg<T> p(img);
	cimg_forZ(img,z) p.get_shared_slice(z) = img.get_slice(z).laplacian();
	p *= 1.0/std::sqrt(20.0);
	CImg<T> X,Y;
	if (robust){
		X = img.get_blur_median(2*scale+1);
		Y = blur_local_variance(p,2*scale+1,1);
	} else {
		X = img.get_blur(scale);
		Y = (p - p.get_blur(scale)).sqr().blur(scale);
	}
	CImg<T> A(X.size(),2);
	A.get_shared_row(0).fill(1);
	A.get_shared_row(1) = X.get_unroll('x');
	CImg<> fit = (A.transpose().pseudoinvert()) * Y.get_unroll('y');
	double g0,edc;
	linear_fit(X,Y,edc,g0,true);
	fit = CImg<>::vector(edc,g0);
	if (visu) {
		X.get_append(Y,'c').display("(E[Y];Var[Y])",false);
		CImg<> X2=CImg<>::vector(1.05*X.min(),0.95*X.max()), Y2= fit(1) * X2 + fit(0);
//		CFigure fig;
//		fig.plot(X,Y,"b+").plot(X2,Y2,"r-").set_axis().display();	//TODO
		cimg_foroff(Y,i) Y(i) = (Y(i)-fit(0))/fit(1);
		X.get_append(Y,'c').display("(E[Y];Var[Y]) normalized",false);
	}
	return fit;
}

//! stabilize the noise variance of a CCD sensor
/**
   \param img the input image
   \param parameters a vector containing [g0,edc]
   \param invert indicate if the inverse transform should be applied
   \note Take into account the assymptotic bias (-1/4)
 **/
template<typename T>
CImg<T> stabilize_ccd_noise(const CImg<T> &img, const CImg<T> &parameters, const bool invert=false){
	const T g0 = parameters(1), edc = parameters(0);
	CImg<T> dest(img);
	if (invert){
		cimg_for(dest,ptr,T) {
			const T val = (*ptr);
			(*ptr) = (T)( g0*(0.25 * val * val - 0.375) - edc / g0 + 0.25);
		}
	} else {
		cimg_for(dest, ptr, T) {
			const T a = g0 * (*ptr) + 0.375 * g0 * g0 + edc, sign = a > 0.0 ? 1.0 : -1.0;
			(*ptr) = (T)(sign*2.0/g0*std::sqrt(cimg::max(g0*(*ptr) + 0.375*g0*g0 + edc,0.0)));
		}
	}
	return dest;
}



//! Crop a patch in the image around position x,y,z and return a column vector
/**
   \param img the image
   \param x x-coordinate of the center of the patch
   \param y y-coordinate of the center of the patch
   \param z z-coordinate of the center of the patch
   \param px the patch half width
   \param px the patch half height
   \param px the patch half depth
   \return img.get_crop(x0,y0,z0,x1,y1,z1).unroll('y');
 **/
template<typename T>
CImg<T> get_patch(const CImg<T> & img,
		int x, int y, int z,
		int px, int py, int pz){
	if (img.depth()==1){
		const int x0 = x - px, y0 = y - py, x1 = x + px, y1 = y + py;
		return img.get_crop(x0,y0,x1,y1).unroll('y');
	} else {
		const int
		x0 = x - px, y0 = y - py, z0 = z - pz,
		x1 = x + px, y1 = y + py, z1 = z + pz;
		return img.get_crop(x0,y0,z0,x1,y1,z1).unroll('y');
	}
}

//! Add a patch to the image
/**
   \param img the image
   \param img the patch as a 1D column vector
   \param x x-coordinate of the center of the patch
   \param y y-coordinate of the center of the patch
   \param z z-coordinate of the center of the patch
   \param px the patch half width
   \param px the patch half height
   \param px the patch half depth
 **/
template<typename T>
void add_patch(CImg<T> & img, CImg<T> & count,
		const CImg<T> &patch,
		int xi, int yi, int zi,
		int px, int py, int pz, const T weight=1.0){
	if (!(img.width()==count.width() && img.height()==count.height() && img.depth() == count.depth()))
		throw CImgException("add_patch: img and count have not the same size");
	if ((int)patch.size()!=(2*px+1)*(2*py+1)*(2*pz+1)*img.spectrum())
		throw CImgException("add_patch: patch has wrong dimension %dx%dx%dx%d",
				patch.width(),patch.height(),patch.depth(),patch.spectrum());
	if (img.depth()==1){
		int i = 0;
		const int x0 = xi - px, y0 = yi - py, x1 = xi + px, y1 = yi + py;
		for (int c = 0; c < img.spectrum(); c++){
			for (int y = y0; y <= y1; y++){
				for (int x = x0; x <= x1; x++){
					if (img.containsXYZC(x,y,0,c)){
						img(x,y,0,c) += weight * patch(i);
						count(x,y,0,c) += weight;
					}
					i++;
				}
			}
		}
	} else {
		const int
		x0 = xi - px, y0 = yi - py, z0 = zi - pz,
		x1 = xi + px, y1 = yi + py, z1 = zi + pz;
		int i=0;
		for (int c = 0; c < img.spectrum(); c++){
			for (int z = z0; z <= z1; z++){
				for (int y = y0; y <= y1; y++){
					for (int x = x0; x <= x1; x++){
						if (img.containsXYZC(x,y,z,c)){
							img(x,y,z,c) += weight * patch(i);
							count(x,y,z,c) += weight;
						}
						i++;
					}
				}
			}
		}
	}
}

//! Gradient of img
/**
   \param img the input image
   \note a backward differences scheme is used and the size of the
   list depends on the dimension of the image.
 **/
template<typename T>
CImgList<T> tv_grad(const CImg<T> &img){
	return img.depth()>1?img.get_gradient("xyz",-1):img.get_gradient("xy",-1);
}

//! Divergence of the field img
/**
   \param img an image list whose elements are the components of the field
   \note The divergence defined as the opposite of the dual of the gradient operator.
   \f$ \textrm{grad}^* = - div \f$
   we use here then forward differences
 **/
template<typename T>
CImg<T> tv_div(const CImgList<T>& img){
	CImg<T> dest(img[0].width(),img[0].height(),img[0].depth(),1,0);
	const char * dir[3] = {"x","y","z"};
	cimglist_for(img,l){
		CImgList<T> g = img[l].get_gradient(dir[l],1);
		dest += g[0];
	}
	return dest;
}

//! norm of the field
/**
   \param img an image list whose elements are the components of the field
 **/
template<typename T>
CImg<T> tv_norm(const CImgList<T> & img, float epsilon=0.f){
	CImg<T> norm(img[0].width(),img[0].height(),img[0].depth());
	cimg_foroff(norm,i) {
		norm(i) = epsilon * epsilon;
		cimglist_for(img,l) norm(i) += img[l](i) * img[l](i);
		norm(i) = sqrt(norm(i));
	}
	return norm;
}

//! Total-Variation denoising using the Chambolle Algorithm
/**
   \param img the input image
   \param lambda the regularization parameter
   \param max_iter the number of iterations
   \param tau_tv

   At each iteration performs:
   \f$ g_{n+1} = \frac{grad(div(g_n))-f/\lambda}{1+\tau \|g_n\|} \f$
   and finally return \f$ f - \lambda div(g) \f$
 **/
template<typename T>
CImg<T> denoise_chambolle_total_variation(const CImg<T> img, const float lambda,
		const int max_iter = 5,
		const float tau_tv = 0.249f){
	CImgList<T> g(2,img.width(),img.height(),img.depth(),1,0);
	for (int i = 0; i < max_iter; i++) {
		CImgList<T> ug = tv_grad(tv_div(g) - img / lambda);
		CImg<T> D =  1.f + tau_tv * tv_norm(ug);
		cimglist_for(g,l){
			g[l] += tau_tv * ug[l];
			g[l].div(D);
		}
	}
	return img - lambda * tv_div(g);
}

//! Tikhonov denoising using PDE approach
/**
   Minimize \f$ \|u - u_0\|^2 + \lambda \| \nabla u \|^2\f$
   using the Euler-Lagrange equation
   \f$ u_{n+1} = u_n + dt/\lambda ( u_0-u_n+\lambda \Delta u_n )\f$
 **/
template<typename T>
CImg<T> denoise_tikhonov(const CImg<T> & img,
		const float lambda = 1,
		const float dt = .25,
		const int max_iter=100){
	if (img.is_empty()) throw CImgArgumentException("denoise_tikonov: empty image.");
	CImg<T> u(img);
	for (int i = 0; i < max_iter; i++)
		u += (dt / lambda) * (img - u + lambda * u.get_laplacian());
	return u;
}

//! Haar wavelet denoising
/**
   \note Need a recent version of CImg (>1.5.1)
 **/
template<typename T>
CImg<T> denoise_haar(const CImg<T> & img, float noise_std=-1,
		float threshold=3.f, int cycles=10, int nscales=32){
	float sigma;
	if (noise_std < 0) sigma = std::sqrt(img.variance_noise());
	else sigma = noise_std;
	int nj = cimg::min(ceil(log2(cimg::min(img.width(),img.height())))-1,
			nscales>0?nscales:32);
	int nx = pow(2,nj+1);
	CImg<T> tmp(img.get_resize(cimg::round(img.width(),nx,1),
			cimg::round(img.height(),nx,1),
			img.depth()==1?1:cimg::round(img.depth(),nx,1),
					img.spectrum(),0));
	CImg<T> dest(tmp.width(),tmp.height(),tmp.depth(),tmp.spectrum(),0);
	for (int i = 0; i < cycles; i++){
		int dx,dy,dz;
		if (i == 0){
			dx = 0; dy = 0; dz = 0;
		} else {
			dx = 4 * nj * cimg::rand();
			dy = 4 * nj * cimg::rand();
			dz = img.depth()==1?0:4 * nj * cimg::rand();
		}
		dest += tmp.get_shift(dx,dy,dz,0,2)
    		  .haar(false, nj-2)
    		  .threshold(threshold * sigma, true)
    		  .haar(true, nj-2)
    		  .shift(-dx,-dy,dz,0,2);
	}
	dest /= (T) cycles;
	dest.resize(img,0);
	return dest;
}




//! Translation Invariant Wavelet transform
/**
   \param img the input image
   \param nj the number of scales
   \return the wavelet coefficients as a CImgList<T> the last one is the approximation
 **/
template<typename T>
CImgList<T> tiwt(const CImg<T> &img, int nj){
	CImgList<T> waves;
	CImg<T> A1(img), A2(img);
	CImg<T> filter;
	for (int scale = 0; scale < nj; scale++){
		int a = 1 << scale, b = 1 << (scale + 1);
		filter.assign(2 * b + 1).fill(0);
		filter(b) = 6.0 / 16.0;
		filter(b + a) = filter(b - a) = 4.0 / 16.0;
		filter(0) = filter(2 * b) =  1.0 / 16.0;
		if (img.width() != 0) A2.convolve(filter.get_unroll('x'));
		if (img.height() != 0) A2.convolve(filter.get_unroll('y'));
		if (img.depth() != 0) A2.convolve(filter.get_unroll('z'));
		waves.push_back(A1 - A2);
		A1 = A2;
	}
	waves.push_back(A1);
	return waves;
}

//! Translation Invariant Inverse Wavelet transform
/**
   \param waves the input wavelet coeffiecients
 **/
template<typename T>
CImg<T> itiwt(const CImgList<T> &waves){
	CImg<> dest(waves[0].width(),waves[0].height(),waves[0].depth(),waves[0].spectrum(),0);
	cimglist_for(waves,l) dest += waves[l];
	return dest;
}

//! Translation Invariant Wavelet Denoising
template<typename T>
CImg<T> denoise_tiwt(const CImg<T> &img, int nj, float threshold){
	CImgList<T> waves = tiwt(img, nj);
	cimglist_for(waves,l) if(l<waves.width()-2) waves[l].threshold(threshold,true);
	return itiwt(waves);
}

//! Extract a local patch dictionnary around point xi,yi,zi
template<typename T>
CImg<T> get_patch_dictionnary(const CImg<T> & img,
		const int xi, const int yi, const int zi,
		const int px, const int py, const int pz,
		const int wx, const int wy, const int wz,
		int & idc){
	CImg<T> S((2*wx+1)*(2*wy+1)*(2*wz+1),(2*px+1)*(2*py+1)*(2*pz+1)*img.spectrum(),1,1,0);
	int idx = 0;
	cimg_forXYZ_window(img,xi,yi,zi,xj,yj,zj,wx,wy,wz){
		CImg<T> patch = get_patch(img, xj, yj, zj, px, py, pz);
		cimg_forY(S,y) S(idx,y) = patch(y);
		if (xj==xi && yj==yi && zj==zi) idc = idx;
		idx++;
	}
	S.columns(0,idx-1);
	return S;
}

template <typename T>
T sum_of_square_differences(const CImg<T> &A, const CImg<T> &B){
	T d = 0;
	cimg_foroff(A, i){ T di = A(i) - B(i); d += di * di; }
	return d;
}

//! Patch based denoising with Wilson-Hilferty transform for the modeling the distance between patches
/**
     \param image
     \param px the patch half width
     \param px the patch half height
     \param px the patch half depth
     \param wx the training region half width
     \param wy the training region half height
     \param wz the training region half depth
     \param nstep the subsampling of the image domain
     \param lambda
     \param noise_std noise level (-1:estimation)
     \param ntype noise type 0: gaussian 1: poisson
     \param transform apply a Wilson-Hilferty transformation to the distances
     \note

     This is an implementation of the non-local mean algorithm.
     Patches are collected in a neighborhood and the squared distance
     to the central patch is computed. The distance should follow a
     Chi square law and a Wilson-Hilferty transform is applied to
     normalize it. The probability that two random patches are similar
     (P(X=Y)) is related to the probability that the distance is less
     than a certain value (P(D<d)) and thus to the cumulative
     distribution of the distances.

 **/
template<typename T>
CImg<T> denoise_patch(const CImg<T> & img,
		const int px, const int py, const int pz,
		const int wx, const int wy, const int wz,
		const int nstep,
		const float lambda, const float noise_std=-1, const bool ntype=0, const bool transform=true){
	const int nd = (2*px+1)*(2*py+1)*(2*pz+1)*img.spectrum();
	const T m = 1.0 - 2.0 / (9.0 * nd), s = std::sqrt(2.0 / (9.0 * nd));
	T sigma;
	if (noise_std < 0) sigma = std::sqrt(img.variance_noise());
	else sigma = noise_std;
	CImg<T> dest(img.width(),img.height(),img.depth(),img.spectrum(),0), count(dest);
#pragma omp parallel for collapse(3)
	cimg_for_stepZ(img,zi,(img.depth()==1||pz==0)?1:nstep){
		cimg_for_stepXY(img,xi,yi,nstep){
			int idc = 0;
			CImg<T> S = get_patch_dictionnary(img,xi,yi,zi,px,py,pz,wx,wy,wz,idc);
			CImg<T> ssd(S.width());
			CImg<T> Sc = S.get_column(idc);
			if (ntype==0)
				cimg_forX(S,x) {
				ssd(x) = sum_of_square_differences(Sc, S.get_column(x)) / (lambda * sigma * sigma);
			} else {
				const T vidc = S.get_column(idc).variance(2);
				cimg_forX(S,x){ ssd(x) = sum_of_square_differences(Sc, S.get_column(x)) / (lambda * vidc);}
			}
			int j = 0;
			T wmax = 0;
			cimg_forXYZ_window(img,xi,yi,zi,xj,yj,zj,wx,wy,wz){
				if (j!=idc){
					const T weight = transform?0.5 - 0.5 * erf(((std::pow(ssd(j)/nd,1.0/3.0)  - m) / s) / std::sqrt(2)) : exp(-.5*ssd(j));
					wmax = cimg::max(wmax, weight);
					add_patch(dest, count, S.get_column(j), xi, yi, zi, px, py, pz, weight);
					add_patch(dest, count, S.get_column(idc), xj, yj, zj, px, py, pz, weight);
				}
				j++;
			}
			add_patch(dest, count, S.get_column(idc), xi, yi, zi, px, py, pz, wmax);
		}
	}
	cimg_foroff(dest,i)
	if(count(i) != 0) dest(i) /= count(i);
	else dest(i) = img(i);
	return dest;
}

//! CHLPCA denoising from the PhD thesis of Hu Haijuan
/**
   \param image
   \param px the patch half width
   \param px the patch half height
   \param px the patch half depth
   \param wx the training region half width
   \param wy the training region half height
   \param wz the training region half depth
   \param nstep the subsampling of the image domain
   \param nsim the number of patches used for training as a factor of the patch size
   \param lambda_min the threshold on the eigen values of the PCA for dimension reduction
   \param threshold the threshold on the value of the coefficients
   \param pca_use_svd if true use the svd approach to perform the pca otherwise use the covariance method
 **/
template<typename T>
CImg<T> denoise_chlpca(const CImg<T> & img,
		const int px, const int py, const int pz,
		const int wx, const int wy, const int wz,
		const int nstep = 5, const float nsim = 6,
		const float lambda_min = 1, const float threshold = -1,
		const float noise_std=-1,  const bool pca_use_svd=true){
	const int
	nd = (2*px+1)*(2*py+1)*(2*pz+1)*img.spectrum(),
	K = nsim*nd;
	float sigma;
	if (noise_std < 0) sigma = std::sqrt(img.variance_noise());
	else sigma = noise_std;
#ifdef DEBUG
//	printf("Patch    \t %dx%d:%d\n",2*px+1,2*py+1,nd);
//	printf("Window   \t %dx%d:%d\n",2*wx+1,2*wy+1,L);
//	printf("nstep    \t %d\n",nstep);
//	printf("nsim     \t %.2g K:%d preselection %s\n"
//			"Threshold\t %.2f,%.2f\n",
//			nsim,K,K<L?"on":"off",lambda_min ,threshold);
#endif
	CImg<T> dest(img), count(img);
	dest.fill(0);
	count.fill(0);
#pragma omp parallel for collapse(3)
	cimg_for_stepZ(img,zi,(img.depth()==1||pz==0)?1:nstep){
		cimg_for_stepXY(img,xi,yi,nstep){
			// extract the training region X
			int idc = 0;
			CImg<T> S = get_patch_dictionnary(img,xi,yi,zi,px,py,pz,wx,wy,wz,idc);
			// select the K most similar patches within the training set
			CImg<T> Sk(S);
			CImg<unsigned int> index(S.width());
			if (K < Sk.width() - 1){
				CImg<T> mse(S.width());
				CImg<unsigned int> perms;
				cimg_forX(S,x){mse(x) = S.get_column(idc).MSE(S.get_column(x)); }
				mse.sort(perms,true);
				cimg_foroff(perms,i) {
					cimg_forY(S,j) Sk(i,j) = S(perms(i),j);
					index(perms(i)) = i;
				}
				Sk.columns(0, K);
				perms.threshold(K);
			} else {
				cimg_foroff(index,i) index(i)=i;
			}
			// centering the patches
			CImg<T> M(1,Sk.height(),1,1,0);
			cimg_forXY(Sk,x,y) M(y) += Sk(x,y);
			M /= (T)Sk.width();
			cimg_forXY(Sk,x,y) Sk(x,y) -= M(y);
			// compute the principal component of the training set S
			CImg<T> P,lambda;
			if (pca_use_svd){
				CImg<T> V;
				Sk.get_transpose().SVD(V,lambda,P,100);
			} else {
				(Sk * Sk.get_transpose()).symmetric_eigen(lambda, P);
				lambda.sqrt();
			}
			//dimension reduction
			int s=0;
			const T tx = std::sqrt((double)Sk.width()-1.0) * lambda_min * sigma;
			while(lambda(s) > tx && s < (int)lambda.size()-1) s++;
			P.columns(0,s);
			// project all the patches on the basis (compute scalar product)
			Sk = P.get_transpose() * Sk;
			// threshold the coefficients
			if (threshold > 0) Sk.threshold(threshold, 1);
			// project back to pixel space
			Sk =  P * Sk;
			// recenter the patches
			cimg_forXY(Sk,x,y) Sk(x,y) += M(y);
			int j = 0;
			cimg_forXYZ_window(img,xi,yi,zi,xj,yj,zj,wx,wy,wz){
				const int id = index(j);
				if (id < Sk.width())
					add_patch(dest, count, Sk.get_column(id), xj, yj, zj, px, py, pz);
				j++;
			}
		}
	}
	cimg_foroff(dest,i)
	if(count(i)!=0) dest(i) /= count(i);
	else dest(i) = img(i);
	return dest;
}

template<typename T>
CImg<T> denoise_chlpca(const CImg<T> &img, const int p=3, const int w=10,
		const int nstep=5, const float nsim=10,
		const float lambda_min=2, const int noise_model=0,
		const bool pca_use_svd=true){
	CImg<T> dest(img),params;
	if (noise_model==1){
		params = estimate_ccd_noise_parameters(img,3,false,false);
		dest = stabilize_ccd_noise(img, params);
	}
	dest = denoise_chlpca(dest, p, p, p, w, w, w, nstep, nsim, lambda_min, -1, pca_use_svd);
	if (noise_model==1) dest = stabilize_ccd_noise(dest, params, true);
	return dest;
}

#endif /* _CIMGUTILS_H_ */
