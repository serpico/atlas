/*
 * main.cpp
 *
 *  Created on: Apr 11, 2014
 *      Author: Antoine Basset
 * 
 * Inria Rennes - Bretagne Atlantique
 * Serpico
 * http://serpico.rennes.inria.fr
 */


#include "core.hpp"
#include "optionChecker.hpp"

bool VERBOSE;


int main(int argc, char **argv) {
	using namespace cimg_library;
	using namespace scalespace;
	using namespace atlas;
	TIFFSetWarningHandler(0);

	cimg_usage("Count the number of blobs in a set of square random images at each scale of a set S = {s0 * r^n | n<N}\n");

	/* computation options */
#ifdef cimg_use_openmp
	omp_set_nested(0);
	opt_chk::read_thread_nb(argc, argv, false);
#endif

#ifdef DEBUG_MODE
	VERBOSE = true;
#else
	VERBOSE = cimg_option("-v", false, "Verbose mode");
#endif

	/* sequence options */
	const int side = cimg_option("-side", 1024, "Side of the random image");
	const int length = cimg_option("-count", 16, "Number of images");

	/* scale options */
	const float t_min = cimg_option("-min", 1.F, "Minimum scale");
	const float t_ratio = cimg_option("-ratio", 1.2F, "Ratio between consecutive scales");
	const int t_count = cimg_option("-count", 20, "Number of scales");

	/* output options */
	const char *out_url = cimg_option("-o", "blobs.txt", "Output file");

	/* help mode */
	if(opt_chk::check_help(argc, argv))
		return EXIT_SUCCESS;

	if(VERBOSE)
		printf("\n");


	/* build scale-space */
	if(VERBOSE)
		printf("Building scale-space representation... ");
	CImg<float> ref_blobs(side, side, length, 1, 0.F);
	ref_blobs.noise(1);
#ifdef DEBUG_MODE
	ref_blobs.save_tiff("noiseimg.tif");
#endif
	const ss_rep rep(ref_blobs, t_min/t_ratio, t_ratio, t_count+2);
	if(VERBOSE)
		printf("done.\n\n");

	/* eval criterion */
	if(VERBOSE)
		printf("Counting blobs...\n\n");
	CImg<float> scores(1);
	detect_blobs<false>(rep, CImg<bool>::empty(), scores);
	if(VERBOSE)
		printf("Scale\tCriterion\n");
	ref_blobs.assign(scores.width(), 2);
	cimg_foroff(scores, t) {
		ref_blobs(t, 0) = rep.t[t];
		ref_blobs(t, 1) = scores[t];
		if(VERBOSE)
			printf("%f\t%f\n", ref_blobs(t, 0), ref_blobs(t, 1));
	}
	if(VERBOSE)
		printf("\n\n");

	/* save reference */
	ref_blobs.save(out_url);
	if(VERBOSE)
		printf("Result saved as %s\n\n", out_url);

	return EXIT_SUCCESS;
}
