/*
 * imgLoops.hpp
 *
 *  Created on: 24 oct. 2013
 *      Author: abasset
 */

#ifndef IMGLOOPS_HPP_
#define IMGLOOPS_HPP_

#include "CImg.h"

using namespace std;
using namespace cimg_library;

#define build_it(img, T) T *_##img##_b = (img).begin();
#define img_forti(img, it, T) \
	for(T *it=(img)._data+(img).size()-1; it>=0; --it)


void test_forit(void) {
	CImg<> img(2,3,1,1);
	build_it(img, float);
	cimg_foroff(img, off)
		img[off] = off+1;
	img_forti(img, it, float)
		cout << *it << ' ';
	cout << endl;
#pragma omp parallel for
	img_forti(img, it, float)
		cout << *(it+_img_b) << ' ';
	cout << endl;
}

#endif /* IMGLOOPS_HPP_ */
