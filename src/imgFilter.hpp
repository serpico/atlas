/*
 * imgFilter.hpp
 *
 *  Created on: 9 avr. 2013
 *      Author: abasset
 */

#ifndef IMGFILTER_HPP_
#define IMGFILTER_HPP_


#include <stdio.h>
#include <stdlib.h>
#ifdef __SunOS_
# include <iostream.h>
#else
# include <iostream>
#endif

//#include <omp.h>

#include <gsl/gsl_sf_bessel.h>

#include "CImg.h"
#include "cimgutils.h"

using namespace std;
using namespace cimg_library;


////////////////
// MIRRORIZE //
//////////////

namespace mirrorize {

template<typename T>
CImg<T> expand(const CImg<T>& image) {
	const CImg<float> mirx = image.get_mirror('x');
	const CImg<float> row = mirx.get_append(image, 'x').get_append(mirx, 'x');
	const CImg<float> miry = row.get_mirror('y');
	return miry.get_append(row, 'y').get_append(miry, 'y');
}

template<typename T>
CImg<T> recrop(const CImg<T> image) {
	const size_t width = image.width()/3;
	const size_t height = image.height()/3;
	return image.get_crop(width, height, 2*width-1, 2*height-1);
}

} // namespace mirrorize


//////////////////////
// DERICHE FILTERS //
////////////////////

/**
 * Deriche filters.
 */
namespace deriche_filter {

/**
 * Evaluate horizontal gradient.
 */
template<typename T>
CImg<T> get_gradX(const CImg<T>& sequence, const float sigma=1.F) {
	return sequence.get_deriche(sigma, 1, 'x').deriche(sigma, 0, 'y');
}

/**
 * Evaluate vertical gradient.
 */
template<typename T>
CImg<T> get_gradY(const CImg<T>& sequence, const float sigma=1.F) {
	return sequence.get_deriche(sigma, 1, 'y').deriche(sigma, 0, 'x');
}

/**
 * Evaluate spatial gradient squared norm.
 * N = Ix^2 + Iy^2
 */
template<typename T>
CImg<T> get_grad_norm2(
		const CImg<T>& sequence, const float sigma=1.F,
		const CImg<T>& gradX=CImg<T>::empty(), const CImg<T>& gradY=CImg<T>::empty()) {
	return (gradX?gradX.get_sqr():get_gradX(sequence, sigma).sqr())
			+ (gradY?gradY.get_sqr():get_gradY(sequence, sigma).sqr());
}

/**
 * Evaluate temporal gradient.
 */
template<typename T>
CImg<T> get_gradT(const CImg<T>& sequence, const float sigma=1.F) {
	return sequence.get_deriche(sigma, 1, 'z');
}

/**
 * Evaluate Gaussian.
 */
template<typename T>
CImg<T> get_blur(const CImg<T>& sequence, const float sigma=1.F) {
	return sequence.get_deriche(sigma, 0, 'x').deriche(sigma, 0, 'y');
}

template<typename T>
CImg<T> get_blur_iso(const CImg<T>& sequence, const float sigma=1.F) {
	const CImg<T> symetrized = sequence.get_mirror("xy");
	CImg<T> blur = get_blur(sequence, sigma);
	blur += get_blur(symetrized, sigma).mirror("xy");
	blur *= .5;
	return blur;
}

/**
 * Evaluate Laplacian.
 */
template<typename T>
CImg<T> get_laplacian(const CImg<T>& sequence, const float sigma=1.F) {
	return sequence.get_deriche(sigma, 2, 'x').deriche(sigma, 0, 'y')
			+ sequence.get_deriche(sigma, 2, 'y').deriche(sigma, 0, 'x');
}

template<typename T>
CImg<T> get_laplacian_iso(const CImg<T>& sequence, const float sigma=1.F) {
	const CImg<T> symetrized = sequence.get_mirror("xy");
	CImg<T> laplacian = get_laplacian(sequence, sigma);
	laplacian += get_laplacian(symetrized, sigma).mirror("xy");
	laplacian *= .5;
	return laplacian;
}

/**
 * Evaluate the diffusion coefficient over a sequence.
 * D = It/delta(I)
 * \param sequence the input 2D+t sequence
 * \param sigmaT the temporal standard deviation (unused if gradT specified)
 * \param sigmaXY the spatial standard deviation (unised if gradNorm specified)
 * \param gradT a pointer to the temporal gradient (0 to re-evaluate)
 * \param laplacian a pointer to the spatial laplacian (0 to re-evaluate)
 */
template<typename T>
CImg<T> get_diff_coef(
		const CImg<T>& sequence, const float sigmaT=1.F, const  float sigmaXY=1.F,
		const CImg<T>& gradT=CImg<T>::empty(), const CImg<T>& laplacian=CImg<T>::empty()) {
	if(gradT)
		return gradT.get_div(laplacian?laplacian:get_laplacian(sequence, sigmaXY));
	return get_gradT(sequence, sigmaT).div(laplacian?laplacian:get_laplacian(sequence, sigmaXY));
}

/**
 * Evaluate the normal velocity over a sequence.
 * Vn = |It|/N2(Ix, Iy)
 * \param sequence the input 2D+t sequence
 * \param sigmaT the temporal standard deviation (unused if gradT specified)
 * \param sigmaXY the spatial standard deviation (unised if gradNorm specified)
 * \param gradT a pointer to the temporal gradient (0 to re-evaluate)
 * \param gradNorm a pointer to the spatial squared gradient norm (0 to re-evaluate)
 */
template<typename T>
CImg<T> get_normal_velocity(
		const CImg<T>& sequence, const float sigmaT=1.F, const float sigmaXY=1.F,
		const CImg<T>& gradT=CImg<T>::empty(), const CImg<T>& gradNorm=CImg<T>::empty()) {
	const CImg<T> gradAbs = gradT?gradT.get_abs():get_gradT(sequence, sigmaT).abs();
	const CImg<T> gradSqrt = gradNorm?gradNorm.get_sqrt():get_grad_norm2(sequence, sigmaXY).sqrt();
	return gradAbs.div(gradSqrt);
}

} // namespace deriche_filter


////////////////////////
// VAN VLIET FILTERS //
//////////////////////

/**
 * Van Vliet filters.
 */
namespace vanvliet_filter {

/**
 * Evaluate horizontal gradient.
 */
template<typename T>
CImg<T> get_gradX(const CImg<T>& sequence, const float sigma=1.F) {
	return sequence.get_vanvliet(sigma, 1, 'x').vanvliet(sigma, 0, 'y');
}

/**
 * Evaluate vertical gradient.
 */
template<typename T>
CImg<T> get_gradY(const CImg<T>& sequence, const float sigma=1.F) {
	return sequence.get_vanvliet(sigma, 1, 'y').vanvliet(sigma, 0, 'x');
}

/**
 * Evaluate spatial gradient squared norm.
 * N = Ix^2 + Iy^2
 */
template<typename T>
CImg<T> get_grad_norm2(
		const CImg<T>& sequence, const float sigma=1.F,
		const CImg<T>& gradX=CImg<T>::empty(), const CImg<T>& gradY=CImg<T>::empty()) {
	return (gradX?gradX.get_sqr():get_gradX(sequence, sigma).sqr())
			+ (gradY?gradY.get_sqr():get_gradY(sequence, sigma).sqr());
}

/**
 * Evaluate temporal gradient.
 */
template<typename T>
CImg<T> get_gradT(const CImg<T>& sequence, const float sigma=1.F) {
	return sequence.get_vanvliet(sigma, 1, 'z');
}

/**
 * Evaluate Gaussian.
 */
template<typename T>
CImg<T> get_blur(const CImg<T>& sequence, const float sigma=1.F) {
	return sequence.get_vanvliet(sigma, 0, 'x').vanvliet(sigma, 0, 'y');
}

template<typename T>
CImg<T> get_blur_iso(const CImg<T>& sequence, const float sigma=1.F) {
	const CImg<T> symetrized = sequence.get_mirror("xy");
	CImg<T> blur = get_blur(sequence, sigma);
	blur += get_blur(symetrized, sigma).mirror("xy");
	blur *= .5;
	return blur;
}

/**
 * Evaluate Laplacian.
 */
template<typename T>
CImg<T> get_laplacian(const CImg<T>& sequence, const float sigma=1.F) {
	return sequence.get_vanvliet(sigma, 2, 'x').vanvliet(sigma, 0, 'y')
			+ sequence.get_vanvliet(sigma, 2, 'y').vanvliet(sigma, 0, 'x');
}

template<typename T>
CImg<T> get_laplacian_iso(const CImg<T>& sequence, const float sigma=1.F) {
	const CImg<T> symetrized = sequence.get_mirror("xy");
	CImg<T> laplacian = get_laplacian(sequence, sigma);
	laplacian += get_laplacian(symetrized, sigma).mirror("xy");
	laplacian *= .5;
	return laplacian;
}

/**
 * Evaluate the diffusion coefficient over a sequence.
 * D = It/delta(I)
 * \param sequence the input 2D+t sequence
 * \param sigmaT the temporal standard deviation (unused if gradT specified)
 * \param sigmaXY the spatial standard deviation (unised if gradNorm specified)
 * \param gradT a pointer to the temporal gradient (0 to re-evaluate)
 * \param laplacian a pointer to the spatial laplacian (0 to re-evaluate)
 */
template<typename T>
CImg<T> get_diff_coef(
		const CImg<T>& sequence, const float sigmaT=1.F, const  float sigmaXY=1.F,
		const CImg<T>& gradT=CImg<T>::empty(), const CImg<T>& laplacian=CImg<T>::empty()) {
	if(gradT)
		return gradT.get_div(laplacian?laplacian:get_laplacian(sequence, sigmaXY));
	return get_gradT(sequence, sigmaT).div(laplacian?laplacian:get_laplacian(sequence, sigmaXY));
}

/**
 * Evaluate the normal velocity over a sequence.
 * Vn = |It|/N2(Ix, Iy)
 * \param sequence the input 2D+t sequence
 * \param sigmaT the temporal standard deviation (unused if gradT specified)
 * \param sigmaXY the spatial standard deviation (unised if gradNorm specified)
 * \param gradT a pointer to the temporal gradient (0 to re-evaluate)
 * \param gradNorm a pointer to the spatial squared gradient norm (0 to re-evaluate)
 */
template<typename T>
CImg<T> get_normal_velocity(
		const CImg<T>& sequence, const float sigmaT=1.F, const float sigmaXY=1.F,
		const CImg<T>& gradT=CImg<T>::empty(), const CImg<T>& gradNorm=CImg<T>::empty()) {
	const CImg<T> gradAbs = gradT?gradT.get_abs():get_gradT(sequence, sigmaT).abs();
	const CImg<T> gradSqrt = gradNorm?gradNorm.get_sqrt():get_grad_norm2(sequence, sigmaXY).sqrt();
	return gradAbs.div(gradSqrt);
}

} // namespace vanvliet_filter


//////////////////////
// SAMPLED FILTERS //
////////////////////

/**
 * Discrete sampled filters.
 */
namespace sampled_filter {


/**
 * Evaluate a Gaussian kernel or a derivative of Gaussian kernel (up to order 3).
 * \param sigma the standard deviation
 */
template<int order>
CImg<float> gaussian_kernel(const float sigma);
//	return CImg<float>::empty();

/**
 * Evaluate a derivative of Gaussian kernel (up to order 3).
 * \param sigma the standard deviation
 * \param Gkernel the already evaluated Gaussian kernel (time saving)
 */
template<int order>
CImg<float> gaussian_kernel(const float sigma, const CImg<float>& Gkernel);
//	return CImg<float>::empty();

/**
 * Convolve image with sampled Gaussian kernel.
 */
template<typename T>
CImg<T> get_blur(const CImg<T>& sequence, const float sigma=1.F) {
	const CImg<float> Gkernel = gaussian_kernel<0>(sigma);
	return sequence.get_convolve(Gkernel).convolve(Gkernel.get_transpose());
}

/**
 * Convolve image with sampled LoG kernel.
 */
template<typename T>
CImg<T> get_laplacian(const CImg<T>& sequence, const float sigma=1.F) {

	const CImg<float> Gkernel = gaussian_kernel<0>(sigma);
	const CImg<float> LoGkernel = gaussian_kernel<2>(sigma, Gkernel);

	return sequence.get_correlate(LoGkernel).correlate(Gkernel.get_transpose())
			+ sequence.get_correlate(LoGkernel.get_transpose()).correlate(Gkernel);
}

} // namespace sampled_filter


//////////////////////////////////
// DISCRETE ANALOG OF GAUSSIAN //
////////////////////////////////

/**
 * Discrete analog of the Gaussian filter as defined by Lindeberg.
 */
namespace discrete_gaussian {

//////////////
// KERNELS //
////////////

/**
 * Get discrete analog of Gaussian kernel.
 * \tparam normalize scale-normalize the filter
 * \param sigma the standard deviation
 * \param extent the kernel size (size = 2 * \c extent * sigma + 1)
 */
template<bool normalize>
CImg<double> get_kernel(
		const float sigma,
		const float extent=4) { //TODO normalize by L1 norm of LoG kernel?

	if(sigma==0)
		return CImg<double>(1,1,1,1,1.); // Dirac

	/* eval components */
	const int radius = sigma*extent + .5; // .5 to round
	const float t = sigma*sigma;
	double bessel[radius+1];
	gsl_sf_bessel_In_scaled_array(0, radius, t, bessel); // exp(-t)It(i)
	const double factor = 1./sqrt(gsl_sf_bessel_i0_scaled(2*t)); //sigma

	/* build kernel */
	CImg<double> kernel(2*radius+1);
	kernel[radius] = normalize?factor*bessel[0]:bessel[0];
	for(int i=1; i<=radius; ++i) {
		const double value = normalize?factor*bessel[i]:bessel[i];
		kernel[radius-i] = value;
		kernel[radius+i] = value;
	}

	return kernel;
}

/**
 * Get a family of scale-normalized discrete analog of Gaussian kernels.
 * \tparam normalize scale-normalize the filter
 * \param tmin the lower scale
 * \param tratio the ratio between consecutive scales
 * \param tcount the number of scales
 * \param extent the kernel size (size = 2 * \c extent * sigma + 1)
 */
template<bool normalize>
CImgList<double> get_ss_kernels(
		const float tmin,
		const float tratio, const unsigned int tcount,
		const float extent=4) {

	CImgList<double> family(tcount);

	float t = tmin;
	for(CImgList<double>::iterator i=family.begin(); i!=family.end(); i++, t*=tratio)
		i->assign(get_kernel<normalize>(sqrt(t), extent));

	return family;
}

/**
 * Rotationally symmetric Laplacian kernel defined by Lindeberg.
 */
const CImg<double> laplacian_kernel = CImg<double>(
		3, 3, 1, 1, // dimensions
		1./6, 2./3, 1./6,
		2./3, -10./3, 2./3,
		1./6, 2./3, 1./6);


///////////////////
// CONVOLUTIONS //
/////////////////

/**
 * Get Gaussian-filtered image.
 * \tparam normalize scale-normalize the filter
 * \param image the image to convolve
 * \param sigma the standard deviation
 * \param extent the kernel size (size = 2 * \c extent * sigma + 1)
 */
template<typename T, bool normalize>
CImg<T> get_blur(
		const CImg<T>& image,
		const float sigma,
		const float extent=4) {

	if(sigma<=0)
		return image;

	CImg<double> kernel = get_kernel<normalize>(sigma, extent);
	CImg<T> res = image.get_correlate(kernel); // correlate faster than convolve
	return res.get_correlate(kernel.get_transpose());
}

/**
 * Build the scale-space representation of an image.
 * \tparam normalize scale-normalize the filter
 * \param image the image to convolve
 * \param tmin the lower scale
 * \param tratio the ratio between consecutive scales
 * \param tcount the number of scales
 * \param extent the kernel size (size = 2 * \c extent * sigma + 1)
 */
template<typename T, bool normalize>
CImgList<T> get_ss_representation(
		const CImg<T>& image,
		const float tmin,
		const float tratio, const unsigned int tcount,
		const float extent=4) {

	if(tcount==0)
		return CImgList<T>::empty();

	CImgList<T> ss(tcount);

	float t = tmin;
	for(typename CImgList<T>::iterator i=ss.begin(); i!=ss.end(); i++, t*=tratio) //TODO parallel?
		i->assign(get_blur<T, normalize>(image, sqrt(t), extent));

	return ss;
}

/**
 * Get Laplacian- or Laplacian of Gaussian-filtered image.
 * \tparam normalize scale-normalize the filter
 * \param image the image to convolve
 * \param image the input image
 * \param sigma the Gaussian standard deviation (0 for no smoothing)
 */
template<typename T, bool normalize>
CImg<T> get_LoG(
		const CImg<T>& image,
		const float sigma=0,
		const unsigned int extent=4) {

	if(sigma==0)
		return image.get_convolve(laplacian_kernel);

	return get_blur<T, normalize>(image, sigma, extent).get_convolve(laplacian_kernel);
}

} // namespace discrete_gaussian


/////////////////////////
// ANSCOMBE TRANSFORM //
///////////////////////

/**
 * Generalized Anscombe Transform functions.
 */
namespace GAT {


/**
 * Stabilize the CCD noise using generalized Anscombe transform.
 */
template<typename T>
void stabilize_variance(CImg<T>& img) {
	const CImg<float> tga_params = estimate_ccd_noise_parameters(img);
	img = stabilize_ccd_noise(img, tga_params, true);
}


/**
 * Stabilize the CCD noise using generalized Anscombe transform.
 */
template<typename T>
CImg<T> get_stabilize_variance(const CImg<T>& img) {
	const CImg<float> tga_params = estimate_ccd_noise_parameters(img);
	return stabilize_ccd_noise(img, tga_params, true);
}

} // namespace GAT


#endif /* IMGFILTER_HPP_ */
