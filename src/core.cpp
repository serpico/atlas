/*
 * SLT.cpp
 *
 *  Created on: Apr 11, 2014
 *      Author: Antoine Basset
 * 
 * Inria Rennes - Bretagne Atlantique
 * Serpico
 * http://serpico.rennes.inria.fr
 */


#include "core.hpp"


/**
 * Poisson CDF.
 */
double poisson_CDF(double x, double lambda) {

	double term = 1;
	double sum = term;
	for(unsigned long i=1; i<x; i++) {
		term *= lambda/i;
		sum += term;
	}
	return exp(-lambda)*sum;
}

/**
 * Gaussian CDF (approximation from A&S Handbook).
 */
double gaussian_CDF(double x) {

    /* constants */
    const double a1 = .254829592;
    const double a2 = -.284496736;
    const double a3 = 1.421413741;
    const double a4 = -1.453152027;
    const double a5 = 1.061405429;
    const double p = .3275911;

    // A&S formula 7.1.26
    const int sign = x>=0?1:-1;
    x = fabs(x)/sqrt(2.);
    const double t = 1./(1. + p*x);
    const double y = 1. - (((((a5*t + a4)*t) + a3)*t + a2)*t + a1)*t*exp(-x*x);

    return .5*(1. + sign*y);
}





float atlas::select_scale(
		const cimg_library::CImg<float>& seq,
		const cimg_library::CImg<float>& ref_scores,
		bool verbose) {
	using namespace cimg_library;

	const float tmin = ref_scores(0, 0);
	const float tratio = ref_scores(1, 0)/tmin;
	const unsigned tcount = ref_scores.width();
	const scalespace::ss_rep rep(seq, tmin, tratio, tcount); //TODO more slices
#ifdef DEBUG_MODE
	rep.L.save_tiff("L.tif");
#endif


	/* count blobs */
	CImg<float> scores(1);
	const CImgList<int> blobs = scalespace::detect_blobs<false>(rep, CImg<bool>::empty(), scores, 0);
#ifdef DEBUG_MODE
	scalespace::draw_blobs(rep.L[0], blobs).save_tiff("blobs.tif");
#endif

	/* evaluate criterion */
	float min = 1;
	int bestt = -1;
	cimg_foroff(scores, t) {
		float scoret = scores[t];
		if(scoret>0) {
			const float mu = ref_scores(t, 1);
			const int inset = sqrt(rep.t[t])+.5;
			const unsigned area = (seq.width()-2*inset)*(seq.height()-2*inset);
			const float criterion = 1.F - poisson_CDF(scoret*area, mu*area);
			if(verbose)
				printf("%f\t%f\t%d\t%f\t%f\n", rep.t[t], mu, area, scoret, criterion);
			if(criterion<min) {
				min = criterion;
				bestt = t;
			}
		}
	}

	return bestt<0?-1:rep.t[bestt];
}

cimg_library::CImg<float> atlas::enhance_objects(const cimg_library::CImg<float>& img, float scale) {
//	return sampled_filter::get_laplacian(img, sqrt(scale));
	using namespace cimg_library;

	const float stdev = sqrt(scale);
	const unsigned length = img.depth();
	CImgList<float> blur(length, img.width(), img.height(), 1, 1);
#pragma omp parallel for
	for(unsigned z=0; z<length; z++)
		blur[z] = sampled_filter::get_laplacian(img.get_slice(z), stdev);
	return blur.get_append('z');
}

cimg_library::CImg<bool> atlas::threshold_gaussian(
		const cimg_library::CImg<float>& img,
		float pval,
		float sigma) {

	const float threshold = approx::gauss_l_quantile(pval);

	def_dimensions(img, i);
	CImg<bool> map(i_w, i_h, i_d, i_s, false);

	/* global threshold */
	if(sigma<=0.1) {
		float mean;
		const float var = omp4cimg::par_meanvar(img, mean); //TODO slice by slice
#pragma omp parallel for
		for(unsigned off=0; off<img.size(); off++)
			if(img[off]<mean + sqrt(var)*threshold)
				map[off] = true;
		return map;
	}

	/* Gaussian weights */
	const CImg<float> mean = deriche_filter::get_blur_iso(img, sigma);
	const CImg<float> img2 = img.get_sqr();
	const CImg<float> mu2 = deriche_filter::get_blur_iso(img2, sigma);
	CImg<float> border(i_w, i_h, 1, 1, 1.F);
	border.deriche(sigma, 0, 'x', false).deriche(sigma, 0, 'y', false);
	border.resize(i_w, i_h, i_d, i_s, 1);
#ifdef DEBUG_MODE
	mean.save("mean.tif");
	(mu2-mean.get_mul(mean)).save("mu2.tif");
	border.save("border.tif");
	(mean + (mu2-mean.get_sqr()).get_sqrt()*threshold)
//			.get_div(border)
			.save_tiff("threshold.tif");
#endif

	/* threshold */
	CImg<float> like(i_w, i_h, i_d, i_s);
#pragma omp parallel for
	for(unsigned off=0; off<img.size(); off++) {
		const float lmean = mean[off];
		const float lvar = mu2[off] - lmean*lmean;
		const float lthresh = lmean + sqrt(lvar)*threshold;
		if(img[off]*border[off]<lthresh) //TODO borders
			map[off] = true;
		like[off] = gaussian_CDF((img[off]-lmean)/sqrt(lvar));
	}
#ifdef DEBUG_MODE
	like.save_tiff("like.tif");
#endif

	return map;
}
