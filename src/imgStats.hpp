/*
 * imgStats.hpp
 *
 *  Created on: 5 avr. 2013
 *      Author: abasset
 */

#ifndef IMGSTATS_HPP_
#define IMGSTATS_HPP_

#include <stdio.h>
#include <stdlib.h>
#ifdef __SunOS_
# include <iostream.h>
#else
# include <iostream>
#endif

#include "CImg.h"
#include "imgColor.hpp"
#include "imgNeighborhood.hpp"
#include "imgSubset.hpp"

using namespace std;
using namespace cimg_library;

/**
 * Print progression: XX% (XX/XX caption)
 * \param current current index
 * \param total total indices count
 * \param inc increment the current index
 * \param caption an additional text after the ratio
 */
void print_progress_temp(size_t& current, const size_t total, bool inc=false, const char *caption=0) {
#pragma omp critical
{
	if(inc)
		current++;
	cout << "\t" << current*100/total << " % (" << current << "/" << total;
	if(caption)
		cout << " " << caption;
	cout << ")\r" << flush;
}
}

/**
 * Draw and save a histogram.
 * \param img the input image
 * \param filename the output file name
 * \param width the output image width
 * \param height the output image width
 * \param bin_nb the number of bins
 * \param min the minimum value to plot
 * \param min the maximum value to plot
 * \param foreground the histogram color
 * \param background the background color (0 for white)
 */
template<typename T>
CImg<unsigned char> draw_histogram(const CImg<T>& img,
		int width, int height,
		int bins_nb, T min=0, T max=0,
		const unsigned char *foreground=color_uchar::black, const unsigned char *background=0) {

	const CImg<unsigned> hist = img.get_histogram(bins_nb, min, max>min?max:img.max());
	CImg<unsigned char> graph(width, height, 1, 3, 255);
	if(background)
		cimg_forXY(graph, x, y)
			cimg_forC(graph, c)
				graph(x, y, 1, c) = background[c];
	return graph.draw_graph(hist, foreground, 1, 3, 0, hist.max(), 0);
}

/**
 * Compute the weighted averaged image of an (img, weights) couple.
 * \param img the raw image
 * \param weights_map the weights map
 * \param min the minimum relevant value of img (qualitative box)
 * \param max the maximum relevant value of img (qualitative box)
 * \param radius the averaging radius
 * \param threshold the denominator minimum value
 * \param compute_residuals compute the (weighted_img-img)^2 map
 * \retun the weighted image and optionally the residuals
 */
template<norm_type N>
CImgList<float> weighted_average(
		const CImg<float>& img, const CImg<float>& weights_map,
		float radius, bool gaussian=false,
		float threshold=0,
		bool compute_residuals=true) {

	/* weighted image */
	CImgList<float> map(compute_residuals?2:1, img.width(), img.height(), img.depth(), img.spectrum(), cimg::type<float>::max());

	/* sum(weights*img)/max(threshold, sum(weights)) */
#pragma omp parallel for
	cimg_forXYZC(img, x, y, z, c) {
		float num = 0.F;
		float denom = 0.F;
		ImgNeighborhood<N> window(x, y, radius, img);	// averaging neighborhood
		img_for_neighborhood(window, i, j) {
			const float value = img(i, j, z, c);
			const float weight = gaussian?weights_map(i, j, z, c)*window.gaussianWeight(x, y):weights_map(i, j, z, c);
			num += weight*value;
			denom += weight;
		}
		if(denom>0) {
			if(denom<threshold)
				denom = threshold;
		} else if(denom>-threshold)
			denom = -threshold;
		map(0, x, y, z, c) = num/denom;
	}

	if(compute_residuals) {
		CImg<float> residuals(map(0));
		cimg_foroff(residuals, off)
			residuals[off] -= img[off];
		residuals.sqr().mul(weights_map);
#pragma omp parallel for
		cimg_forXYZC(img, x, y, z, c) {
			float diff = 0.F;
			ImgNeighborhood<N> window(x, y, radius, img);
			img_for_neighborhood(window, i, j) {
				const float weight = gaussian?window.gaussianWeight(x, y):1;
				diff += weight*residuals(i, j, z, c);
			}
			map(1, x, y, z, c) = diff;
		}
	}
	return map;
}

/**
 * Linear least square fitting of Y = aX.
 * \param Y the Y image
 * \param X the X image
 * \param radius the radius of the neighborhood used
 * \param threshold the denominator minimum value
 * \param compute_residuals compute the (weighted_img-img)^2 map
 * \retun the weighted image and optionally the residuals
 */
template<norm_type N>
CImgList<float> linear_least_square(
		const CImg<float>& Y, const CImg<float>& X,
		float radius, bool gaussian=false,
		float threshold=0,
		bool compute_residuals=true) {

	CImgList<float> a(compute_residuals?2:1, Y.width(), Y.height(), Y.depth(), Y.spectrum(), cimg::type<float>::max());

#pragma omp parallel for
	cimg_forXYZC(Y, x, y, z, c) {
		float num = 0.F;
		float denom = 0.F;
		ImgNeighborhood<N> window(x, y, radius, Y);	// data neighborhood
		img_for_neighborhood(window, i, j) {
			float value = X(i, j, z, c);
			const float weight = gaussian?window.gaussianWeight(x, y):1;
			num += value*weight*Y(i, j, z, c);
			denom += value*value*weight;
		}
		if(denom>0) {
			if(denom<threshold)
			denom = threshold;
		} else if(denom>-threshold)
			denom = -threshold;
		a(0, x, y, z, c) = num/denom;
	}

	if(compute_residuals) {
		CImg<float> residuals(Y);
		cimg_foroff(residuals, off)
			residuals[off] -= a(0)[off]*X[off];
		residuals.sqr();
#pragma omp parallel for
		cimg_forXYZC(Y, x, y, z, c) {
			float diff = 0.F;
			ImgNeighborhood<N> window(x, y, radius, Y);
			img_for_neighborhood(window, i, j) {
				const float weight = gaussian?window.gaussianWeight(x, y):1;
				diff += weight*residuals(i, j, z, c);
			}
			a(1, x, y, z, c) = diff;
		}
	}
	return a;
}

/**
 * Linear least square fitting of Y = aX.
 * \param Y the Y image
 * \param X the X image
 * \param subset the sets of pixels
 * \param threshold the denominator minimum value
 * \param compute_residuals compute the sum of the squared residuals for each label
 * \return the a vector (label_nb) and optionally the sum of the squared residuals
 */
//template<norm_type N, typename T>
//CImgList<float> linear_least_square(
//		const CImg<float>& Y, const CImg<float>& X,
//		const ImgSubsetList& subset,
//		const float threshold=0,
//		const bool compute_residuals=false) {
//
//	const unsigned count = subset.size();	// number of labels
//	CImgList<float> a(compute_residuals?2:1, count);
//
//#pragma omp parallel for
//	for(int l=0; l<count; ++l) { // for every label
//
//		/* evaluate a */
//		float num = 0.F;
//		float denom = 0.F;
//		img_for_subset(subset[l], i, x, y, z, c) { // for every labeled point
//			float value = X(x, y, z);
//			num += value*Y(x, y, z);
//			denom += value*value;
//		}
//		a(0, l) = num/(denom<threshold?threshold:denom);
//
//		/* evaluate sum of squared errors */
//		if(compute_residuals) {
//			float sse = 0.F;	// sum of squared errors
//			img_for_subset(subset[l], i, x, y, z, c) {
//				const float res = Y(x, y, z) - a(0, z, l-1)*X(x, y, z);	// residual
//				sse += res*res;
//			}
//			a(1, l-1) = sse;
//		}
//	}
//
//	return a;
//}

/**
 * Linear least square fitting of Y = aX.
 * \param Y the Y image
 * \param X the X image
 * \param objects the sets of objects
 * \param threshold the denominator minimum value
 * \param compute_residuals compute the sum of the squared residuals for each label
 * \return the a vector (label_nb) and optionally the sum of the squared residuals
 */
template<norm_type N>
CImgList<float> linear_least_square(
		const CImg<float>& Y, const CImg<float>& X,
		const ImgSubseqSet& objects,
		const float threshold=0,
		const bool compute_residuals=false,
		const size_t t_integration=0) {

	CImgList<float> a(compute_residuals?2:1, objects.size(), objects.length());

#pragma omp parallel for
	for(size_t l=0; l<objects.size(); ++l) { // for every label
		for(size_t z=0; z<objects.length(); ++z) { // for every frame

			/* evaluate a */
			float num = 0.F;
			float denom = 0.F;
			img_for_subsetXY(objects[l][z], i, x, y) { // for every labeled point
				for(int t=z>t_integration?z-t_integration:size_t(0); t<=min(z+t_integration, objects.length()-1); ++t) {
					float value = X(x, y, t);
					num += value*Y(x, y, t);
					denom += value*value;
				}
			}
			a(0, l, z) = num/(denom<threshold?threshold:denom);

			/* evaluate sum of squared errors */
			if(compute_residuals) {
				float sse = 0.F;	// sum of squared errors
				img_for_subsetXY(objects[l][z], i, x, y) {
					for(int t=z>t_integration?z-t_integration:size_t(0); t<min(z+t_integration, objects.length()); ++t) {
						const float res = Y(x, y, t) - a(0, l, t)*X(x, y, t);	// residual
						sse += res*res;
					}
				}
				a(1, l, z) = sse;
			}
		}
	}

	return a;
}

void LK_for_subset(const CImgList<float>& gradients, const ImgSubsetXY& subset, float& vx, float& vy, const int z=0) {

	CImg<float> tensor(2, 2);
	float sumx=0, sumy=0, sumxy=0, sumxt=0, sumyt=0;
	const CImg<float>& gradX = gradients[0];
	const CImg<float>& gradY = gradients[1];
	const CImg<float>& gradT = gradients[2];

	img_for_subsetXY(subset, i, xi, yi) {
		const float gx = gradX(xi, yi, z);
		const float gy = gradY(xi, yi, z);
		const float gt = gradT(xi, yi, z);
		sumx += gx*gx;
		sumy += gy*gy;
		sumxy += gx*gy;
		sumxt -= gx*gt;
		sumyt -= gy*gt;
	}
	tensor(0, 0) = sumx;
	tensor(0, 1) = sumxy;
	tensor(1, 0) = sumxy;
	tensor(1, 1) = sumy;
	tensor.invert();

	vx = sumxt*tensor(0, 0) + sumyt*tensor(0, 1);
	vy = sumxy*tensor(1, 0) + sumyt*tensor(1, 1);
}

CImgList<float> LK_for_objects(
		const CImgList<float>& gradients, const ImgSubseqSet& objects,
		const bool compute_residuals=false) {

	CImgList<float> v(compute_residuals?3:2, objects.size(), objects.length(), 1, 1, 0.F);

#pragma omp parallel for
	for(size_t l=0; l<objects.size(); ++l) { // for every label
		for(size_t z=0; z<objects.length(); ++z) { // for every frame
			const ImgSubsetXY& subset = objects[l][z];
			if(subset) {
				float vx, vy;
				LK_for_subset(gradients, subset, vx, vy, z);
				v(0, l, z) = vx;
				v(1, l, z) = vy;

				if(compute_residuals) {
					img_for_subsetXY(subset, i, xi, yi) {
							const float residual =
									gradients(2, xi, yi, z)
									+ gradients(0, xi, yi, z)*vx
									+ gradients(1, xi, yi, z)*vy;	// It + Ix.Vx + Iy.Vy
							v(2, l, z) += residual*residual;
						}
				}
			}
		}
	}

	return v;
}

/**
 * Compute the minimal distances between patches of a sequence.
 * \param img the sequence
 * \param window_rad the research windows radius
 * \param patch_rad the blocs radius
 * \param time_step the step between current and previous/next frame
 */
template<typename T>
CImg<float> dist_min(
		const CImg<T>& sequence,
		float window_rad, float patch_rad, int time_step=1) {

	/* borders */
	const float inset = window_rad+patch_rad;

	/* distance minimum map */
	CImg<T> dist_map(sequence, "xyz", 0);

	/* dist_min = min_window(d(central_patch, current_patch)) */
#pragma omp parallel for
	cimg_forXY(dist_map, x, y) {	//TODO also evaluate boundaries

		/* research window and compared patch */
		const ImgWindow window(x, y, window_rad, window_rad);
		const ImgWindow centralPatch(x, y, patch_rad, patch_rad);
		ImgWindow patch = centralPatch;

		cimg_forZ(dist_map, z) {

			/* distance minimum */
			T dist_min = cimg::type<T>::max();
			img_for_window(window, i, j) {
				patch.setCenter(i, j, true);
				if(z>=time_step) {
					T dist_prev = centralPatch.distToWindow2(sequence, patch, z, z-time_step);	//TODO test L1
					if(dist_prev<dist_min)
						dist_min = dist_prev;
				}
				if(z<sequence.depth()-time_step) {
//				else {	//TODO test
					T dist_next = centralPatch.distToWindow2(sequence, patch, z, z+time_step);	//TODO idem
					if(dist_next<dist_min)
						dist_min = dist_next;
				}
			}
			dist_map(x, y, z) = dist_min;
		}
	}
	return dist_map;
}


/**
 * Get p-quantile of the distribution of a sample image.
 * \param outcomes the sample image
 * \param p_value the p-value
 */
template<typename T>
T eval_quantile(const CImg<T>& outcomes, float p_value, bool smallest=false, size_t offset=0) {
	const size_t total = outcomes.size()-offset;
	const size_t limit = total*p_value;
	return outcomes.get_sort(smallest)[limit+offset];
}

template<typename T>
T eval_mean_over(const CImg<T>& outcomes, const CImg<bool>& support) {
	size_t count = 0;
	T mean = 0;
	cimg_foroff(outcomes, off) {
		if(support[off]) {
			count++;
			mean += outcomes[off];
		}
	}
	return mean/count;
}

template<typename T>
T eval_sum_over(const CImg<T>& outcomes, const CImg<bool>& support) {
	T sum = 0;
	cimg_foroff(outcomes, off)
		if(support[off])
			sum += outcomes[off];
	return sum;
}

template<typename T>
T eval_quantile_over(const CImg<T>& outcomes, const CImg<bool>& support, float p_value, bool smallest=false) {
	size_t count = 0;
	cimg_foroff(support, off)
		if(support[off])
			count++;
	CImg<T> restriction(count);
	count = 0;
	cimg_foroff(support, off) {
		if(support[off]) {
			restriction[count] = outcomes[off];
			count++;
		}
	}
	return eval_quantile(restriction, p_value, smallest);
}

template<typename T>
T eval_median_over(const CImg<T>& outcomes, const CImg<bool>& support) {
	return eval_quantile_over(outcomes, support, 0.5F);
}

/**
 * Adapted from http://www.dandiggins.co.uk/arlib-9.html
 */
template<typename T>
T threshold_otsu(const CImg<T>& img, const int bins=256) {
	// NOTE: Creation of histogram[256] not shown

	float w = 0;	// first order cumulative
	float u = 0;	// second order cumulative
	float  uT = 0;	// total mean level

	CImg<float> histNormalized = img.get_histogram(bins);///img.size();	// normalized histogram values
	histNormalized /= img.size();
	histNormalized.save("hist.tif");

	float  work1, work2;	// working variables
	double work3 = 0.0;

	// Calculate total mean level
	cimg_foroff(histNormalized, off)
		uT += (off+1)*histNormalized[off];


	// Find optimal threshold value
	for(size_t off=0; off<histNormalized.size()-1; ++off) {
	    w += histNormalized[off];
	    u += (off+1)*histNormalized[off];
	    work1 = (uT * w - u);
	    work2 = (work1 * work1) / ( w * (1.F-w) );
	    if (work2>work3)
	    	work3=work2;
	}

	// Convert the final value to an integer
	const T min = img.min();
	const T max = img.max();
	return (int(sqrt(work3))+.5)*(max-min)/bins+min;
}

template<typename T>
CImg<T> binarize_otsu(CImg<T>& img, const int bins=256) {
	return img.threshold(threshold_otsu(img, bins));
}

template<typename T>
CImg<T> get_binarize_otsu(const CImg<T>& img, const int bins=256) {
	return img.get_threshold(threshold_otsu(img, bins));
}


#endif /* IMGSTATS_HPP_ */
